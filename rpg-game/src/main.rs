use std::{
    collections::HashSet,
    num::NonZeroU32,
    path::{Path, PathBuf},
};

use futures::executor::block_on;
use rpg_engine::{
    bevy_ecs::{
        schedule::{ParallelSystemDescriptorCoercion, Schedule},
        system::IntoSystem,
    },
    App, MainStage, RenderStage, StartupStage, UpdateStage,
};
use rpg_world::data::{Id, MapEntity, Master, PlayerRef, Size};
use tracing::{debug, error};
use tracing_subscriber::{
    prelude::__tracing_subscriber_SubscriberExt, util::SubscriberInitExt, EnvFilter,
};

mod collisions;
mod conversion;
mod systems;

fn register_logging() {
    #[cfg(all(profiling, chrome))]
    let (chrome, _guard) = tracing_chrome::ChromeLayerBuilder::new().build();
    let fmt = tracing_subscriber::fmt::Layer::default();
    let filter = EnvFilter::from_default_env();

    let registry = tracing_subscriber::registry();

    #[cfg(not(profiling))]
    let registry = registry.with(fmt);

    #[cfg(not(profiling))]
    let registry = registry.with(filter);

    #[cfg(all(profiling, chrome))]
    let registry = registry.with(chrome);

    registry.init();
}

async fn setup() -> Result<App, LoadWorldError> {
    let world = load_world()?;

    Ok(App::builder()
        .add_default_systems()
        .await
        .stage(MainStage::Update, |schedule: &mut Schedule| {
            schedule.add_system_to_stage(UpdateStage::Update, systems::camera.system())
        })
        .stage(MainStage::Startup, |schedule: &mut Schedule| {
            schedule.add_system_to_stage(StartupStage::Startup, systems::load_assets.system())
        })
        .add_system_to_stage(MainStage::Prepare, systems::update_map.system())
        .add_system_to_stage(MainStage::Input, systems::input.system())
        .stage(MainStage::Render, |schedule: &mut Schedule| {
            schedule
                .add_system_to_stage(
                    RenderStage::PreRender,
                    systems::collision.system().label("Collision"),
                )
                .add_system_to_stage(
                    RenderStage::PreRender,
                    systems::movement.system().after("Collision"),
                )
        })
        // .insert_resource(RenderPass::<MapPass>::default())
        .insert_resource(world)
        .build())
}

#[derive(Debug)]
enum LoadWorldError {
    Io(std::io::Error),
    InvalidFileFormat(PathBuf),
    NoMaster,
}

pub struct World {
    master: Master,
}

impl World {
    fn get_all_textures(&self, textures: &mut HashSet<String>) {
        self.master.get_all_textures(textures);
    }

    fn get_active_map(&self) -> Id {
        self.master.get_active_map()
    }

    fn get_map(&self, id: Id) -> Option<(Size<NonZeroU32>, Vec<MapEntity>)> {
        self.master.get_map(id)
    }

    fn get_player_id(&self) -> &PlayerRef {
        self.master.get_player_id()
    }
}

fn load_world() -> Result<World, LoadWorldError> {
    let data_dir = Path::new("data/");

    let loadorder = data_dir.join("loadorder");

    let loadorder = std::fs::read_to_string(loadorder).map_err(|e| LoadWorldError::Io(e))?;

    debug!(?loadorder, "loadorder");

    let mut master: Option<Master> = None;

    for file in loadorder.lines() {
        if file.ends_with(".bwm") {
            let file = data_dir.join(file);
            if let Some(_master1) = master {
                error!(
                    master = ?file,
                    "Multiple Master Files in loadorder"
                );
            }

            let bytes = std::fs::read(&file).map_err(|e| LoadWorldError::Io(e))?;
            master = Some(Master::from_bytes(&bytes[..]).map_err(|err| {
                error!(?err);
                LoadWorldError::InvalidFileFormat(file)
            })?);
        } else if file.ends_with(".jwm") {
            let file = data_dir.join(file);
            if let Some(_master1) = master {
                error!(
                    master = ?file,
                    "Multiple Master Files in loadorder"
                );
            }
            let bytes = std::fs::read(&file).map_err(|e| LoadWorldError::Io(e))?;
            master = Some(Master::from_json(&bytes[..]).map_err(|err| {
                error!(?err);
                LoadWorldError::InvalidFileFormat(file)
            })?);
        }
    }

    if let Some(master) = master {
        Ok(World { master })
    } else {
        Err(LoadWorldError::NoMaster)
    }
}

fn main() {
    register_logging();

    let app = block_on(setup()).unwrap();

    app.run();
}
