use std::{collections::HashMap, time::Duration};

use rpg_engine::{cgmath::Vector2, image::DynamicImage, Handle};

pub(crate) fn convert_animation(
    animation: &rpg_world::data::Animation,
    lookup: &HashMap<String, Handle<DynamicImage>>,
) -> Option<rpg_engine::animations::Animation> {
    Some(match animation {
        rpg_world::data::Animation::Single(i) => {
            rpg_engine::animations::Animation::Frame(*lookup.get(i)?)
        }
        rpg_world::data::Animation::BlendSpace(i) => {
            rpg_engine::animations::Animation::BlendSpace2(convert_blendspace(i, lookup)?)
        }
        rpg_world::data::Animation::StateMashine(i) => {
            rpg_engine::animations::Animation::StateMashine(convert_statemashine(i, lookup)?)
        }
        rpg_world::data::Animation::KeyFrames(i) => {
            rpg_engine::animations::Animation::KeyFrameAnimation(convert_keyframe_animation(
                i, lookup,
            )?)
        }
    })
}

fn convert_blendspace(
    animation: &rpg_world::data::BlendSpace,
    lookup: &HashMap<String, Handle<DynamicImage>>,
) -> Option<rpg_engine::animations::BlendSpace2> {
    let animations: Result<Vec<_>, _> = animation
        .points
        .iter()
        .map(|a| convert_blendspace_child(&a, lookup).ok_or(()))
        .collect();

    Some(rpg_engine::animations::BlendSpace2::new(
        animations.ok()?,
        convert_parameter(&animation.parameter, lookup)?,
    ))
}

fn convert_blendspace_child(
    animation: &rpg_world::data::BlendSpaceChild,
    lookup: &HashMap<String, Handle<DynamicImage>>,
) -> Option<rpg_engine::animations::BlendSpace2Child> {
    Some(rpg_engine::animations::BlendSpace2Child {
        position: Vector2::new(animation.position.x, animation.position.y),
        animation: convert_animation(&animation.child, lookup)?,
    })
}

fn convert_parameter(
    animation: &rpg_world::data::BlendSpaceParameter,
    _lookup: &HashMap<String, Handle<DynamicImage>>,
) -> Option<rpg_engine::animations::BlendSpace2Parameter> {
    Some(match animation {
        rpg_world::data::BlendSpaceParameter::Vector(v) => {
            rpg_engine::animations::BlendSpace2Parameter::Vector(v.clone())
        }
        rpg_world::data::BlendSpaceParameter::Values(x, y) => {
            rpg_engine::animations::BlendSpace2Parameter::Values(x.clone(), y.clone())
        }
    })
}

fn convert_statemashine(
    animation: &rpg_world::data::AnimationStateMashine,
    lookup: &HashMap<String, Handle<DynamicImage>>,
) -> Option<rpg_engine::animations::StateMashine> {
    let mut builder = rpg_engine::animations::StateMashine::builder();

    let mut nodes_cache = HashMap::new();
    for (index, nodes) in &animation.nodes {
        let (inner, name) = convert_statemashine_node(&nodes, lookup)?;
        let id = if animation.start_node == *index {
            builder.add_start_node(inner, name)
        } else if animation.end_nodes.contains(index) {
            builder.add_end_node(inner, name)
        } else {
            builder.add_node(inner, name)
        };
        nodes_cache.insert(index, id);
    }

    for edge in &animation.edged {
        let from = nodes_cache.get(&edge.from)?;
        let to = nodes_cache.get(&edge.to)?;

        builder.add_transition(
            *from,
            *to,
            rpg_engine::animations::Transition {
                priority: edge.weight,
                transition_type: match edge.transition_type {
                    rpg_world::data::TransitionType::Immediate => {
                        rpg_engine::animations::TransitionType::Immediate
                    }
                    rpg_world::data::TransitionType::Finished => {
                        rpg_engine::animations::TransitionType::Finished
                    }
                },
            },
        );
    }

    Some(builder.build()?)
}

fn convert_statemashine_node(
    animation: &rpg_world::data::AnimationStateMashineNode,
    lookup: &HashMap<String, Handle<DynamicImage>>,
) -> Option<(rpg_engine::animations::Animation, String)> {
    Some((
        convert_animation(&animation.child, lookup)?,
        animation.name.clone(),
    ))
}

fn convert_keyframe_animation(
    animation: &rpg_world::data::KeyFrameAnimation,
    lookup: &HashMap<String, Handle<DynamicImage>>,
) -> Option<rpg_engine::animations::KeyFrameAnimation> {
    let frames: Result<Vec<_>, ()> = animation
        .frames
        .iter()
        .map(|f| convert_animation_frame(f, lookup).ok_or(()))
        .collect();

    Some(rpg_engine::animations::KeyFrameAnimation::new(
        frames.ok()?,
        match animation.repeat {
            rpg_world::data::Repeat::None => rpg_engine::animations::Repeat::None,
            rpg_world::data::Repeat::Infinite => rpg_engine::animations::Repeat::Infinite,
        },
    ))
}

fn convert_animation_frame(
    animation: &rpg_world::data::AnimationKeyFrame,
    lookup: &HashMap<String, Handle<DynamicImage>>,
) -> Option<rpg_engine::animations::KeyFrame> {
    Some(rpg_engine::animations::KeyFrame {
        frame: *lookup.get(&animation.frame)?,
        duration: Duration::from_millis(animation.duration),
    })
}
