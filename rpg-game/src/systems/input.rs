use std::time::Duration;

use rpg_engine::{
    animations::AnimationParameters,
    bevy_ecs::prelude::{Query, Res, With},
    cgmath::{InnerSpace, Vector2, Zero},
    WinitInputHelper,
};

use super::{Player, Velocity};

const MOVEMENT_SPEED: f32 = 6.0;

pub fn input(
    input: Res<WinitInputHelper>,
    mut player: Query<(&mut Velocity, &mut AnimationParameters), With<Player>>,
    d_time: Res<Duration>,
) {
    if let Ok((mut player, mut parameters)) = player.single_mut() {
        let mut velocity = Vector2::new(0.0, 0.0);

        if input.key_held(rpg_engine::VirtualKeyCode::Up) {
            velocity.y = -1.0;
        }

        if input.key_held(rpg_engine::VirtualKeyCode::Down) {
            velocity.y = 1.0;
        }

        if input.key_held(rpg_engine::VirtualKeyCode::Left) {
            velocity.x = -1.0;
        }

        if input.key_held(rpg_engine::VirtualKeyCode::Right) {
            velocity.x = 1.0;
        }

        if input.key_pressed(rpg_engine::VirtualKeyCode::Return) {}

        if velocity.magnitude2() != 0.0 {
            parameters.set_vector("input".to_string(), velocity);

            parameters.set_string("target".to_string(), "walking".to_string());

            let new_vel = velocity.normalize() * MOVEMENT_SPEED * d_time.as_secs_f32();

            player.0 = new_vel;
        } else {
            parameters.set_string("target".to_string(), "idle".to_string());
            player.0 = Vector2::zero();
        }
    }
}
