use rpg_engine::bevy_ecs::prelude::Query;

use super::{MapPosition, Velocity};

pub fn movement(mut actors: Query<(&mut MapPosition, &Velocity)>) {
    for (mut pos, vel) in actors.iter_mut() {
        pos.x += vel.0.x;
        pos.y += vel.0.y;
    }
}
