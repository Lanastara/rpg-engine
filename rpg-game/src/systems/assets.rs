use std::collections::{HashMap, HashSet};

use tracing::instrument;

use rpg_engine::{
    assets::{AssetServer, Assets},
    bevy_ecs::prelude::{Commands, Res, ResMut},
    graphics::backend::Graphics,
    image::DynamicImage,
    texture_atlases::{uniform::UniformTextureAtlas, TextureAtlasHandle, TextureAtlases},
    Handle,
};

use crate::World;

use super::MapId;

pub struct Textures(
    pub TextureAtlasHandle,
    pub HashMap<String, Handle<DynamicImage>>,
);

#[instrument(skip(commands, asset_server, assets, graphics, texture_atlases, world))]
pub fn load_assets(
    mut commands: Commands,
    mut asset_server: ResMut<AssetServer>,
    assets: Res<Assets>,
    graphics: Res<Graphics>,
    texture_atlases: ResMut<TextureAtlases>,
    world: Res<World>,
) {
    let mut textures = HashSet::new();
    world.get_all_textures(&mut textures);

    let texture_lookup: HashMap<_, _> = textures
        .iter()
        .map(|t| {
            (
                t.clone(),
                asset_server.load::<DynamicImage>(&format!("data/textures/{}", &t)),
            )
        })
        .collect();

    let mut builder = UniformTextureAtlas::builder((16, 16));

    for (_, texture) in texture_lookup.iter() {
        builder.add_texture(*texture);
    }

    let atlas = builder.build(assets.clone(), &graphics.device);

    let map_texture_atlas = texture_atlases.insert(atlas);

    commands.insert_resource(Textures(map_texture_atlas, texture_lookup));
    commands.insert_resource(MapId(None, world.get_active_map()))
}
