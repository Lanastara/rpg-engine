use rpg_engine::{
    animations::AnimationParameters,
    bevy_ecs::prelude::{Commands, Entity, Query, Res, ResMut},
    graphics::{AtlasTexture, Render, ScreenPosition, Sprite},
};
use tracing::{info, warn};

use crate::{World, conversion::convert_animation, systems::{Collision, MapInformation, Player, Velocity}};

use super::{MapId, Textures};

pub struct MapPosition {
    pub x: f32,
    pub y: f32,
}

pub struct MapReference(u32);

pub fn update_map(
    world: Res<World>,
    map_texture_atlas: Res<Textures>,
    mut commands: Commands,
    mut map_entities: Query<(&mut MapPosition, &mut Sprite, &mut MapReference, Entity)>,
    mut map_id: ResMut<MapId>,
) {
    match map_id.0 {
        Some(id) if id == map_id.1 => {
            //TODO: Handle incremental changes
        }
        _ => {
            info!("Rebuilding map");

            for (_, _, _, e) in map_entities.iter_mut() {
                commands.entity(e).despawn();
            }

            let texture_atlas_handle = map_texture_atlas.0;

            if let Some((size, entities)) = world.get_map(map_id.1) {
                for entity in entities.iter() {
                    match entity {
                        rpg_world::data::MapEntity::Object(id, object_ref, object) => {
                            let texture = map_texture_atlas.1.get(&object.texture);

                            if let Some(texture) = texture {
                                let mut commands = commands.spawn_bundle((
                                    MapPosition {
                                        x: object_ref.position.x as f32,
                                        y: object_ref.position.y as f32,
                                    },
                                    texture_atlas_handle,
                                    Sprite {
                                        handle: Some(*texture),
                                    },
                                    MapReference(*id),
                                    ScreenPosition::default(),
                                    Render::default(),
                                    AtlasTexture::default(),
                                ));

                                if object.collision{
                                    println!("inserting Collision");
                                    commands.insert(Collision);
                                }
                            } else {
                                warn!(texture = ?object.texture, "Texture not found");
                            }
                        }
                        rpg_world::data::MapEntity::Actor(id, actor_ref, actor) => {
                            if let Some(animation) =
                                convert_animation(&actor.animation, &map_texture_atlas.1)
                            {
                                let mut commands = commands.spawn_bundle((
                                    MapPosition {
                                        x: actor_ref.position.x,
                                        y: actor_ref.position.y,
                                    },
                                    texture_atlas_handle,
                                    Sprite { handle: None },
                                    MapReference(*id),
                                    ScreenPosition::default(),
                                    Render::default(),
                                    AtlasTexture::default(),
                                    animation,
                                    AnimationParameters::default(),
                                ));

                                if world.get_player_id().refernce_id == *id {
                                    commands.insert_bundle((Player, Velocity::default()));
                                }
                            } else {
                                warn!(actor_id = id, "error in animation for actor reference.")
                            }
                        }
                    }
                }

                commands.insert_resource(MapInformation {
                    width: size.width,
                    height: size.height,
                });
            } else {
                warn!(map_id = map_id.1, "Map not found");
            }

            map_id.0 = Some(map_id.1);
        }
    }
}
