use super::CollisionPoint;

use super::dynamic_rect_vs_rect as super_dynamic_rect_vs_rect;
use super::ray_vs_rect as super_ray_vs_rect;
use super::Aabb2;
use rpg_engine::cgmath::{Point2, Vector2};
use test_case::test_case;

#[test_case(Vector2::new(0.5,0.5), Vector2::new( 1.0, 1.0), Vector2::new(1.0, 1.0) => Some(CollisionPoint { contact_point: Point2 { x: 1.0, y: 1.0 }, normal: Vector2 { x: 0.0, y: 0.0 }, t_hit: 0.5 })  )]
#[test_case(Vector2::new(1.5,0.5), Vector2::new( 0.0, 1.0), Vector2::new(1.0, 1.0) => Some(CollisionPoint { contact_point: Point2 { x: 1.5, y: 1.0 }, normal: Vector2 { x: 0.0, y: -1.0 }, t_hit: 0.5 })  )]
#[test_case(Vector2::new(2.5,0.5), Vector2::new(-1.0, 1.0), Vector2::new(1.0, 1.0) => Some(CollisionPoint { contact_point: Point2 { x: 2.0, y: 1.0 }, normal: Vector2 { x: 0.0, y: 0.0 }, t_hit: 0.5 })  )]
#[test_case(Vector2::new(0.5,1.5), Vector2::new( 1.0, 0.0), Vector2::new(1.0, 1.0) => Some(CollisionPoint { contact_point: Point2 { x: 1.0, y: 1.5 }, normal: Vector2 { x: -1.0, y: 0.0 }, t_hit: 0.5 })  )]
#[test_case(Vector2::new(2.5,1.5), Vector2::new(-1.0, 0.0), Vector2::new(1.0, 1.0) => Some(CollisionPoint { contact_point: Point2 { x: 2.0, y: 1.5 }, normal: Vector2 { x: 1.0, y: 0.0 }, t_hit: 0.5 })  )]
#[test_case(Vector2::new(0.5,2.5), Vector2::new( 1.0,-1.0), Vector2::new(1.0, 1.0) => Some(CollisionPoint { contact_point: Point2 { x: 1.0, y: 2.0 }, normal: Vector2 { x: 0.0, y: 0.0 }, t_hit: 0.5 })  )]
#[test_case(Vector2::new(1.5,2.5), Vector2::new( 0.0,-1.0), Vector2::new(1.0, 1.0) => Some(CollisionPoint { contact_point: Point2 { x: 1.5, y: 2.0 }, normal: Vector2 { x: 0.0, y: 1.0 }, t_hit: 0.5 })  )]
#[test_case(Vector2::new(2.5,2.5), Vector2::new(-1.0,-1.0), Vector2::new(1.0, 1.0) => Some(CollisionPoint { contact_point: Point2 { x: 2.0, y: 2.0 }, normal: Vector2 { x: 0.0, y: 0.0 }, t_hit: 0.5 })  )]
#[test_case(Vector2::new(0.5,0.5), Vector2::new( 2.0, 2.0), Vector2::new(1.0, 1.0) => Some(CollisionPoint { contact_point: Point2 { x: 1.0, y: 1.0 }, normal: Vector2 { x: 0.0, y: 0.0 }, t_hit: 0.25 })  )]
#[test_case(Vector2::new(1.5,0.5), Vector2::new( 0.0, 2.0), Vector2::new(1.0, 1.0) => Some(CollisionPoint { contact_point: Point2 { x: 1.5, y: 1.0 }, normal: Vector2 { x: 0.0, y: -1.0 }, t_hit: 0.25 })  )]
#[test_case(Vector2::new(2.5,0.5), Vector2::new(-2.0, 2.0), Vector2::new(1.0, 1.0) => Some(CollisionPoint { contact_point: Point2 { x: 2.0, y: 1.0 }, normal: Vector2 { x: 0.0, y: 0.0 }, t_hit: 0.25 })  )]
#[test_case(Vector2::new(0.5,1.5), Vector2::new( 2.0, 0.0), Vector2::new(1.0, 1.0) => Some(CollisionPoint { contact_point: Point2 { x: 1.0, y: 1.5 }, normal: Vector2 { x: -1.0, y: 0.0 }, t_hit: 0.25 })  )]
#[test_case(Vector2::new(2.5,1.5), Vector2::new(-2.0, 0.0), Vector2::new(1.0, 1.0) => Some(CollisionPoint { contact_point: Point2 { x: 2.0, y: 1.5 }, normal: Vector2 { x: 1.0, y: 0.0 }, t_hit: 0.25 })  )]
#[test_case(Vector2::new(0.5,2.5), Vector2::new( 2.0,-2.0), Vector2::new(1.0, 1.0) => Some(CollisionPoint { contact_point: Point2 { x: 1.0, y: 2.0 }, normal: Vector2 { x: 0.0, y: 0.0 }, t_hit: 0.25 })  )]
#[test_case(Vector2::new(1.5,2.5), Vector2::new( 0.0,-2.0), Vector2::new(1.0, 1.0) => Some(CollisionPoint { contact_point: Point2 { x: 1.5, y: 2.0 }, normal: Vector2 { x: 0.0, y: 1.0 }, t_hit: 0.25 })  )]
#[test_case(Vector2::new(2.5,2.5), Vector2::new(-2.0,-2.0), Vector2::new(1.0, 1.0) => Some(CollisionPoint { contact_point: Point2 { x: 2.0, y: 2.0 }, normal: Vector2 { x: 0.0, y: 0.0 }, t_hit: 0.25 })  )]
#[test_case(Vector2::new(0.5,0.5), Vector2::new( 0.2, 0.2), Vector2::new(1.0, 1.0) => Some(CollisionPoint { contact_point: Point2 { x: 1.0, y: 1.0 }, normal: Vector2 { x: 0.0, y: 0.0 }, t_hit: 2.5 })  )]
#[test_case(Vector2::new(1.5,0.5), Vector2::new( 0.0, 0.2), Vector2::new(1.0, 1.0) => Some(CollisionPoint { contact_point: Point2 { x: 1.5, y: 1.0 }, normal: Vector2 { x: 0.0, y: -1.0 }, t_hit: 2.5 })  )]
#[test_case(Vector2::new(2.5,0.5), Vector2::new(-0.2, 0.2), Vector2::new(1.0, 1.0) => Some(CollisionPoint { contact_point: Point2 { x: 2.0, y: 1.0 }, normal: Vector2 { x: 0.0, y: 0.0 }, t_hit: 2.5 })  )]
#[test_case(Vector2::new(0.5,1.5), Vector2::new( 0.2, 0.0), Vector2::new(1.0, 1.0) => Some(CollisionPoint { contact_point: Point2 { x: 1.0, y: 1.5 }, normal: Vector2 { x: -1.0, y: 0.0 }, t_hit: 2.5 })  )]
#[test_case(Vector2::new(2.5,1.5), Vector2::new(-0.2, 0.0), Vector2::new(1.0, 1.0) => Some(CollisionPoint { contact_point: Point2 { x: 2.0, y: 1.5 }, normal: Vector2 { x: 1.0, y: 0.0 }, t_hit: 2.5 })  )]
#[test_case(Vector2::new(0.5,2.5), Vector2::new( 0.2,-0.2), Vector2::new(1.0, 1.0) => Some(CollisionPoint { contact_point: Point2 { x: 1.0, y: 2.0 }, normal: Vector2 { x: 0.0, y: 0.0 }, t_hit: 2.5 })  )]
#[test_case(Vector2::new(1.5,2.5), Vector2::new( 0.0,-0.2), Vector2::new(1.0, 1.0) => Some(CollisionPoint { contact_point: Point2 { x: 1.5, y: 2.0 }, normal: Vector2 { x: 0.0, y: 1.0 }, t_hit: 2.5 })  )]
#[test_case(Vector2::new(2.5,2.5), Vector2::new(-0.2,-0.2), Vector2::new(1.0, 1.0) => Some(CollisionPoint { contact_point: Point2 { x: 2.0, y: 2.0 }, normal: Vector2 { x: 0.0, y: 0.0 }, t_hit: 2.5 })  )]
#[test_case(Vector2::new(0.5,0.5), Vector2::new( 1.0, 0.0), Vector2::new(1.0, 1.0) => None)]
#[test_case(Vector2::new(0.5,0.5), Vector2::new( 0.0, 1.0), Vector2::new(1.0, 1.0) => None)]
#[test_case(Vector2::new(2.5,0.5), Vector2::new(-1.0, 0.0), Vector2::new(1.0, 1.0) => None)]
#[test_case(Vector2::new(2.5,0.5), Vector2::new(-0.0, 1.0), Vector2::new(1.0, 1.0) => None)]
#[test_case(Vector2::new(0.5,2.5), Vector2::new( 1.0,-0.0), Vector2::new(1.0, 1.0) => None)]
#[test_case(Vector2::new(0.5,2.5), Vector2::new( 0.0,-1.0), Vector2::new(1.0, 1.0) => None)]
#[test_case(Vector2::new(2.5,2.5), Vector2::new(-1.0,-0.0), Vector2::new(1.0, 1.0) => None)]
#[test_case(Vector2::new(2.5,2.5), Vector2::new(-0.0,-1.0), Vector2::new(1.0, 1.0) => None)]
#[test_case(Vector2::new(1.5,1.5), Vector2::new( 0.0, 0.0), Vector2::new(1.0, 1.0) => None)]
fn ray_vs_rect(
    ray_origin: Vector2<f32>,
    ray_direction: Vector2<f32>,
    target: Vector2<f32>,
) -> Option<CollisionPoint> {
    let target = Aabb2 {
        min: Point2::<f32>::new(0.0, 0.0) + target,
        max: Point2::<f32>::new(0.0, 0.0) + target + Vector2::new(1.0, 1.0),
    };
    super_ray_vs_rect(Point2::<f32>::new(0.0, 0.0) + ray_origin, ray_direction, target)
}

#[test_case(Vector2::new(0.0,0.0), Vector2::new(1.0,1.0), Vector2::new(1.0, 1.0) => Some(CollisionPoint { contact_point: Point2 { x: 0.54999995, y: 0.54999995 }, normal: Vector2 { x: 0.0, y: 0.0 }, t_hit: 0.049999952 })  )]
#[test_case(Vector2::new(1.0,0.0), Vector2::new(0.0,1.0), Vector2::new(1.0, 1.0) => Some(CollisionPoint { contact_point: Point2 { x: 1.5, y: 0.54999995 }, normal: Vector2 { x: 0.0, y: -1.0 }, t_hit: 0.049999952 })  )]
#[test_case(Vector2::new(2.0,0.0), Vector2::new(-1.0,1.0), Vector2::new(1.0, 1.0) => Some(CollisionPoint { contact_point: Point2 { x: 2.45, y: 0.54999995 }, normal: Vector2 { x: 0.0, y: 0.0 }, t_hit: 0.049999952 })  )]
#[test_case(Vector2::new(0.0,1.0), Vector2::new(1.0,0.0), Vector2::new(1.0, 1.0) => Some(CollisionPoint { contact_point: Point2 { x: 0.54999995, y: 1.5 }, normal: Vector2 { x: -1.0, y: 0.0 }, t_hit: 0.049999952 })  )]
#[test_case(Vector2::new(2.0,1.0), Vector2::new(-1.0,0.0), Vector2::new(1.0, 1.0) => Some(CollisionPoint { contact_point: Point2 { x: 2.45, y: 1.5 }, normal: Vector2 { x: 1.0, y: 0.0 }, t_hit: 0.049999952 })  )]
#[test_case(Vector2::new(0.0,2.0), Vector2::new(1.0,-1.0), Vector2::new(1.0, 1.0) => Some(CollisionPoint { contact_point: Point2 { x: 0.54999995, y: 2.45 }, normal: Vector2 { x: 0.0, y: 0.0 }, t_hit: 0.049999952 })  )]
#[test_case(Vector2::new(1.0,2.0), Vector2::new(0.0,-1.0), Vector2::new(1.0, 1.0) => Some(CollisionPoint { contact_point: Point2 { x: 1.5, y: 2.45 }, normal: Vector2 { x: 0.0, y: 1.0 }, t_hit: 0.049999952 })  )]
#[test_case(Vector2::new(2.0,2.0), Vector2::new(-1.0,-1.0), Vector2::new(1.0, 1.0) => Some(CollisionPoint { contact_point: Point2 { x: 2.45, y: 2.45 }, normal: Vector2 { x: 0.0, y: 0.0 }, t_hit: 0.049999952 })  )]
#[test_case(Vector2::new(0.0,0.0), Vector2::new(2.0,2.0), Vector2::new(1.0, 1.0) => Some(CollisionPoint { contact_point: Point2 { x: 0.54999995, y: 0.54999995 }, normal: Vector2 { x: 0.0, y: 0.0 }, t_hit: 0.024999976 })  )]
#[test_case(Vector2::new(1.0,0.0), Vector2::new(0.0,2.0), Vector2::new(1.0, 1.0) => Some(CollisionPoint { contact_point: Point2 { x: 1.5, y: 0.54999995 }, normal: Vector2 { x: 0.0, y: -1.0 }, t_hit: 0.024999976 })  )]
#[test_case(Vector2::new(2.0,0.0), Vector2::new(-2.0,2.0), Vector2::new(1.0, 1.0) => Some(CollisionPoint { contact_point: Point2 { x: 2.45, y: 0.54999995 }, normal: Vector2 { x: 0.0, y: 0.0 }, t_hit: 0.024999976 })  )]
#[test_case(Vector2::new(0.0,1.0), Vector2::new(2.0,0.0), Vector2::new(1.0, 1.0) => Some(CollisionPoint { contact_point: Point2 { x: 0.54999995, y: 1.5 }, normal: Vector2 { x: -1.0, y: 0.0 }, t_hit: 0.024999976 })  )]
#[test_case(Vector2::new(2.0,1.0), Vector2::new(-2.0,0.0), Vector2::new(1.0, 1.0) => Some(CollisionPoint { contact_point: Point2 { x: 2.45, y: 1.5 }, normal: Vector2 { x: 1.0, y: 0.0 }, t_hit: 0.024999976 })  )]
#[test_case(Vector2::new(0.0,2.0), Vector2::new(2.0,-2.0), Vector2::new(1.0, 1.0) => Some(CollisionPoint { contact_point: Point2 { x: 0.54999995, y: 2.45 }, normal: Vector2 { x: 0.0, y: 0.0 }, t_hit: 0.024999976 })  )]
#[test_case(Vector2::new(1.0,2.0), Vector2::new(0.0,-2.0), Vector2::new(1.0, 1.0) => Some(CollisionPoint { contact_point: Point2 { x: 1.5, y: 2.45 }, normal: Vector2 { x: 0.0, y: 1.0 }, t_hit: 0.024999976 })  )]
#[test_case(Vector2::new(2.0,2.0), Vector2::new(-2.0,-2.0), Vector2::new(1.0, 1.0) => Some(CollisionPoint { contact_point: Point2 { x: 2.45, y: 2.45 }, normal: Vector2 { x: 0.0, y: 0.0 }, t_hit: 0.024999976 })  )]
#[test_case(Vector2::new(0.0,0.0), Vector2::new(0.2,0.2), Vector2::new(1.0, 1.0) => Some(CollisionPoint { contact_point: Point2 { x: 0.54999995, y: 0.54999995 }, normal: Vector2 { x: 0.0, y: 0.0 }, t_hit: 0.24999976 })  )]
#[test_case(Vector2::new(1.0,0.0), Vector2::new(0.0,0.2), Vector2::new(1.0, 1.0) => Some(CollisionPoint { contact_point: Point2 { x: 1.5, y: 0.54999995 }, normal: Vector2 { x: 0.0, y: -1.0 }, t_hit: 0.24999976 })  )]
#[test_case(Vector2::new(2.0,0.0), Vector2::new(-0.2,0.2), Vector2::new(1.0, 1.0) => Some(CollisionPoint { contact_point: Point2 { x: 2.45, y: 0.54999995 }, normal: Vector2 { x: 0.0, y: 0.0 }, t_hit: 0.24999976 })  )]
#[test_case(Vector2::new(0.0,1.0), Vector2::new(0.2,0.0), Vector2::new(1.0, 1.0) => Some(CollisionPoint { contact_point: Point2 { x: 0.54999995, y: 1.5 }, normal: Vector2 { x: -1.0, y: 0.0 }, t_hit: 0.24999976 })  )]
#[test_case(Vector2::new(2.0,1.0), Vector2::new(-0.2,0.0), Vector2::new(1.0, 1.0) => Some(CollisionPoint { contact_point: Point2 { x: 2.45, y: 1.5 }, normal: Vector2 { x: 1.0, y: 0.0 }, t_hit: 0.24999976 })  )]
#[test_case(Vector2::new(0.0,2.0), Vector2::new(0.2,-0.2), Vector2::new(1.0, 1.0) => Some(CollisionPoint { contact_point: Point2 { x: 0.54999995, y: 2.45 }, normal: Vector2 { x: 0.0, y: 0.0 }, t_hit: 0.24999976 })  )]
#[test_case(Vector2::new(1.0,2.0), Vector2::new(0.0,-0.2), Vector2::new(1.0, 1.0) => Some(CollisionPoint { contact_point: Point2 { x: 1.5, y: 2.45 }, normal: Vector2 { x: 0.0, y: 1.0 }, t_hit: 0.24999976 })  )]
#[test_case(Vector2::new(2.0,2.0), Vector2::new(-0.2,-0.2), Vector2::new(1.0, 1.0) => Some(CollisionPoint { contact_point: Point2 { x: 2.45, y: 2.45 }, normal: Vector2 { x: 0.0, y: 0.0 }, t_hit: 0.24999976 })  )]
#[test_case(Vector2::new(0.0,0.0), Vector2::new(1.0,0.0), Vector2::new(1.0, 1.0) => None)]
#[test_case(Vector2::new(0.0,0.0), Vector2::new(0.0,1.0), Vector2::new(1.0, 1.0) => None)]
#[test_case(Vector2::new(2.0,0.0), Vector2::new(-1.0,0.0), Vector2::new(1.0, 1.0) => None)]
#[test_case(Vector2::new(2.0,0.0), Vector2::new(-0.0,1.0), Vector2::new(1.0, 1.0) => None)]
#[test_case(Vector2::new(0.0,2.0), Vector2::new(1.0,-0.0), Vector2::new(1.0, 1.0) => None)]
#[test_case(Vector2::new(0.0,2.0), Vector2::new(0.0,-1.0), Vector2::new(1.0, 1.0) => None)]
#[test_case(Vector2::new(2.0,2.0), Vector2::new(-1.0,-0.0), Vector2::new(1.0, 1.0) => None)]
#[test_case(Vector2::new(2.0,2.0), Vector2::new(-0.0,-1.0), Vector2::new(1.0, 1.0) => None)]
#[test_case(Vector2::new(1.0,1.0), Vector2::new(0.0,0.0), Vector2::new(1.0, 1.0) => None)]
fn dynamic_rect_vs_rect(
    dynamic_pos: Vector2<f32>,
    velocity: Vector2<f32>,
    static_pos: Vector2<f32>,
) -> Option<CollisionPoint> {
    let dynamic_aabb2 = Aabb2 {
        min: Point2::<f32>::new(0.0, 0.0) + dynamic_pos,
        max: Point2::<f32>::new(0.0, 0.0) + dynamic_pos + Vector2::new(1.0, 1.0),
    };

    let static_aabb2 = Aabb2 {
        min: Point2::<f32>::new(0.0, 0.0) + static_pos + Vector2::new(0.05, 0.05),
        max: Point2::<f32>::new(0.0, 0.0) + static_pos + Vector2::new(0.95, 0.95),
    };
    super_dynamic_rect_vs_rect(dynamic_aabb2, velocity, static_aabb2)
}
