#[derive(Debug, Clone, Copy)]
pub struct Aabb2<T> {
    pub min: Point2<T>,
    pub max: Point2<T>,
}

impl Aabb2<f32> {
    fn width(&self) -> f32 {
        (self.max.x - self.min.x).abs()
    }

    fn height(&self) -> f32 {
        (self.max.y - self.min.y).abs()
    }

    fn half_size(&self) -> Vector2<f32> {
        Vector2::new(self.width() / 2.0, self.height() / 2.0)
    }

    fn center(&self) -> Point2<f32> {
        let min = Point2::new(
            if self.min.x < self.max.x {
                self.min.x
            } else {
                self.max.x
            },
            if self.min.y < self.max.y {
                self.min.y
            } else {
                self.max.y
            },
        );

        min + self.half_size()
    }
}

use rpg_engine::cgmath::{prelude::ElementWise, Point2, Vector2, Zero};

#[cfg(test)]
mod tests;

#[derive(Debug, PartialEq)]
pub struct CollisionPoint {
    contact_point: Point2<f32>,
    normal: Vector2<f32>,
    pub t_hit: f32,
}

fn partial_min<T: PartialOrd<T> + Copy>(v1: &Vector2<T>, v2: &Vector2<T>) -> Vector2<T> {
    Vector2::new(
        if v1.x < v2.x { v1.x } else { v2.x },
        if v1.y < v2.y { v1.y } else { v2.y },
    )
}

fn partial_max<T: PartialOrd<T> + Copy>(v1: &Vector2<T>, v2: &Vector2<T>) -> Vector2<T> {
    Vector2::new(
        if v1.x > v2.x { v1.x } else { v2.x },
        if v1.y > v2.y { v1.y } else { v2.y },
    )
}

fn partial_min_point<T: PartialOrd<T> + Copy>(v1: &Point2<T>, v2: &Point2<T>) -> Point2<T> {
    Point2::new(
        if v1.x < v2.x { v1.x } else { v2.x },
        if v1.y < v2.y { v1.y } else { v2.y },
    )
}

fn partial_max_point<T: PartialOrd<T> + Copy>(v1: &Point2<T>, v2: &Point2<T>) -> Point2<T> {
    Point2::new(
        if v1.x > v2.x { v1.x } else { v2.x },
        if v1.y > v2.y { v1.y } else { v2.y },
    )
}

fn ray_vs_rect(
    ray_origin: Point2<f32>,
    ray_direction: Vector2<f32>,
    target: Aabb2<f32>,
) -> Option<CollisionPoint> {
    let inv_direction = Vector2::new(1.0, 1.0).div_element_wise(ray_direction);

    let near = (target.min - ray_origin).mul_element_wise(inv_direction);
    let far = (target.max - ray_origin).mul_element_wise(inv_direction);

    if near.x.is_nan() && near.y.is_nan() && far.x.is_nan() && far.y.is_nan() {
        return None;
    }

    let (near, far): (Vector2<f32>, Vector2<f32>) =
        { (partial_min(&near, &far), partial_max(&near, &far)) };

    if near.x > far.y || near.y > far.x {
        return None;
    }

    let t_hit_near = if near.x > near.y { near.x } else { near.y };
    let t_hit_far = if far.x < far.y { far.x } else { far.y };

    if t_hit_far < 0.0 {
        return None;
    }

    let contact_point = ray_origin + t_hit_near * ray_direction;

    let normal = if near.x > near.y {
        if inv_direction.x < 0.0 {
            Vector2::new(1.0, 0.0)
        } else {
            Vector2::new(-1.0, 0.0)
        }
    } else if near.x < near.y {
        if inv_direction.y < 0.0 {
            Vector2::new(0.0, 1.0)
        } else {
            Vector2::new(0.0, -1.0)
        }
    } else {
        Vector2::zero()
    };

    Some(CollisionPoint {
        normal,
        contact_point,
        t_hit: t_hit_near,
    })
}

pub fn dynamic_rect_vs_rect(
    dynamic_aabr: Aabb2<f32>,
    velocity: Vector2<f32>,
    static_aabr: Aabb2<f32>,
) -> Option<CollisionPoint> {
    if velocity.x == 0.0 && velocity.y == 0.0 {
        return None;
    }

    let half_size = dynamic_aabr.half_size();

    let expanded_target = Aabb2 {
        min: static_aabr.min - half_size,
        max: static_aabr.max + half_size,
    };

    ray_vs_rect(dynamic_aabr.center(), velocity, expanded_target).and_then(|col| {
        if col.t_hit >= 0.0 && col.t_hit < 1.0 {
            Some(col)
        } else {
            None
        }
    })
}

pub fn resolve_dynamic_rect_vs_rect(
    dynamic_aabr: Aabb2<f32>,
    velocity: Vector2<f32>,
    static_aabr: Aabb2<f32>,
) -> Option<Vector2<f32>> {
    if let Some(CollisionPoint { normal, t_hit, .. }) =
        dynamic_rect_vs_rect(dynamic_aabr, velocity, static_aabr)
    {
        let velocity = velocity + normal.mul_element_wise(velocity.map(f32::abs)) * (1.0 - t_hit);
        return Some(velocity);
    }

    None
}

pub fn hit_test_broad_bb(pos: Point2<f32>, velocity: Vector2<f32>) -> Aabb2<f32> {
    let old_pos = pos.map(|f| f.floor());
    let new_pos = (pos + velocity).map(|f| f.floor());

    let min: Point2<f32> = partial_min_point(&old_pos, &new_pos);
    let max: Point2<f32> = partial_max_point(&old_pos, &new_pos) + Vector2::new(1.0, 1.0);

    Aabb2 { min, max }
}
