use rpg_engine::{
    bevy_ecs::prelude::{Query, Res, With},
    cgmath::{Vector2, Zero},
    graphics::{Render, ScreenPosition},
    WindowSize,
};

use super::{MapInformation, MapPosition, Player};

pub fn camera(
    mut query: Query<(&MapPosition, &mut ScreenPosition, &mut Render)>,
    player: Query<&MapPosition, With<Player>>,
    screen_size: Res<WindowSize>,
    map: Res<MapInformation>,
) {
    const TILE_WIDTH: i32 = 32;
    let cam_pos = player
        .single()
        .map(|p| Vector2::new(p.x, p.y))
        .unwrap_or(Vector2::zero());

    let n_visible_tiles_x = screen_size.width as i32 / TILE_WIDTH;
    let n_visible_tiles_y = screen_size.height as i32 / TILE_WIDTH;

    let mut offset = Vector2::new(
        cam_pos.x - n_visible_tiles_x as f32 / 2.0,
        cam_pos.y - n_visible_tiles_y as f32 / 2.0,
    );

    if offset.x < -1.0 {
        offset.x = -1.0
    }
    if offset.y < -1.0 {
        offset.y = -1.0
    }

    if offset.x > (u32::from(map.width) as i32 + 1 - n_visible_tiles_x) as f32 {
        offset.x = (u32::from(map.width) as i32 + 1 - n_visible_tiles_x) as f32
    }

    if offset.y > (u32::from(map.height) as i32 + 1 - n_visible_tiles_y) as f32 {
        offset.y = (u32::from(map.height) as i32 + 1 - n_visible_tiles_y) as f32
    }

    let int_offset = Vector2::new(offset.x as i32, offset.y as i32);

    for (map, mut screen, mut render) in query.iter_mut() {
        render.0 = map.x >= (offset.x - 1.0)
            && map.y >= (offset.y - 1.0)
            && map.x <= (int_offset.x + n_visible_tiles_x) as f32
            && map.y <= (int_offset.y + n_visible_tiles_y) as f32;

        screen.scale = Vector2::new(TILE_WIDTH as f32, TILE_WIDTH as f32);
        screen.position = Vector2::new(
            (map.x - offset.x) * TILE_WIDTH as f32,
            (map.y - offset.y) * TILE_WIDTH as f32,
        );
    }
}
