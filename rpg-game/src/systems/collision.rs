use std::{cmp::Ordering, ops::DerefMut};

use std::ops::Deref;
use tracing::instrument;

use rpg_engine::{
    bevy_ecs::prelude::{Entity, Query, With},
    cgmath::{Point2, Vector2},
};

use math::{
    dynamic_rect_vs_rect, hit_test_broad_bb, resolve_dynamic_rect_vs_rect, Aabb2, CollisionPoint,
};

use super::{MapPosition, Velocity};

#[cfg(test)]
mod tests;

mod math;

fn inner_collision(
    position: &MapPosition,
    velocity: &mut Velocity,
    entity: Entity,
    collisions_query: &Query<(&MapPosition, Entity), With<super::Collision>>,
) {
    let position = Point2::new(position.x, position.y);

    let dynamic_aabb = Aabb2 {
        min: position + Vector2::new(0.2, 0.2),
        max: position + Vector2::new(0.8, 0.8),
    };

    let mut collisions: Vec<(CollisionPoint, Aabb2<f32>)> = Vec::new();

    let hit_test_broad = hit_test_broad_bb(position, velocity.0);

    for (c_position, c_entity) in collisions_query.iter() {
        if entity == c_entity
        || (c_position.x < hit_test_broad.min.x
            || c_position.y < hit_test_broad.min.y
            || c_position.x > hit_test_broad.max.x
            || c_position.y > hit_test_broad.max.y)
        {
            continue;
        }

        let static_aabb = Aabb2 {
            min: Point2::<f32>::new(c_position.x, c_position.y),
            max: Point2::<f32>::new(c_position.x + 1.0, c_position.y + 1.0),
        };

        if let Some(collision) = dynamic_rect_vs_rect(dynamic_aabb, velocity.0, static_aabb) {
            collisions.push((collision, static_aabb));
        }
    }

    // dbg!(&collisions);

    collisions.sort_unstable_by(|col1, col2| {
        if col1.0.t_hit < col2.0.t_hit {
            Ordering::Less
        } else {
            Ordering::Greater
        }
    });

    for (_, static_aabb) in collisions {
        if let Some(vel) = resolve_dynamic_rect_vs_rect(dynamic_aabb, velocity.0, static_aabb) {
            velocity.0 = vel;
        }
    }
}

#[instrument(skip(actors, collisions))]
pub fn collision(
    mut actors: Query<(&MapPosition, &mut Velocity, Entity)>,
    collisions: Query<(&MapPosition, Entity), With<super::Collision>>,
) {
    for (position, mut velocity, entity) in actors.iter_mut() {
        inner_collision(position.deref(), velocity.deref_mut(), entity, &collisions);
    }
}
