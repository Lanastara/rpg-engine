use rpg_engine::cgmath::Zero;
use std::num::NonZeroU32;

mod assets;
mod camera;
mod collision;
mod input;
mod map;
mod movement;

pub use assets::*;
pub use camera::*;
pub use collision::*;
pub use input::*;
pub use map::*;
pub use movement::*;
use rpg_engine::cgmath::Vector2;

pub struct MapId(Option<u32>, u32);
pub struct MapInformation {
    width: NonZeroU32,
    height: NonZeroU32,
}

pub struct Player;

pub struct Collision;

#[derive(Debug, Clone)]
pub struct Velocity(Vector2<f32>);

impl Default for Velocity {
    fn default() -> Self {
        Self(Vector2::zero())
    }
}
