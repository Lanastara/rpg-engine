use serde::{Deserialize, Serialize};
use std::{
    collections::{HashMap, HashSet},
    num::NonZeroU32,
};
use tracing::warn;

pub type Id = u32;

#[derive(Debug, Serialize, Deserialize)]
pub struct Master {
    pub globals: Globals,
    pub actors: HashMap<Id, Actor>,
    pub objects: HashMap<Id, Object>,
    pub maps: HashMap<Id, Map>,
}

pub enum MapEntity<'a> {
    Object(Id, &'a ObjectRef, &'a Object),
    Actor(Id, &'a ActorRef, &'a Actor),
}

impl Master {
    pub fn from_bytes(bytes: &[u8]) -> Result<Self, Box<bincode::ErrorKind>> {
        bincode::deserialize(bytes)
    }

    pub fn from_json(json: &[u8]) -> Result<Self, serde_json::Error> {
        serde_json::from_slice(json)
    }

    pub fn get_all_textures(&self, textures: &mut HashSet<String>) {
        for (_id, a) in &self.actors {
            a.get_all_textures(textures);
        }

        for (_, object) in &self.objects {
            object.get_all_textures(textures);
        }

        for (_, map) in &self.maps {
            map.get_all_textures(textures);
        }
    }

    pub fn get_active_map(&self) -> Id {
        self.globals.player.map_id
    }

    pub fn get_map(&self, id: Id) -> Option<(Size<NonZeroU32>, Vec<MapEntity>)> {
        let map = self.maps.get(&id);

        match map {
            Some(map) => {
                let mut ret = Vec::with_capacity(map.actor_refs.len() + map.object_refs.len());

                for (id, actor_ref) in map.actor_refs.iter() {
                    if let Some(actor) = self.actors.get(&actor_ref.actor_id) {
                        ret.push(MapEntity::Actor(*id, actor_ref, actor));
                    } else {
                        warn!(actor_id = actor_ref.actor_id, "missing Actor");
                    }
                }

                for (id, object_ref) in map.object_refs.iter() {
                    if let Some(object) = self.objects.get(&object_ref.object_id) {
                        ret.push(MapEntity::Object(*id, object_ref, object));
                    } else {
                        warn!(object_id = object_ref.object_id, "missing Object");
                    }
                }

                Some((map.size, ret))
            }
            None => None,
        }
    }

    pub fn get_player_id(&self) -> &PlayerRef {
        &self.globals.player
    }
}

#[derive(Debug, Serialize, Deserialize)]
pub struct Globals {
    pub player: PlayerRef,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct PlayerRef {
    pub map_id: Id,
    pub refernce_id: Id,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct Actor {
    pub animation: Animation,
}

impl Actor {
    pub fn get_all_textures(&self, textures: &mut HashSet<String>) {
        self.animation.get_all_textures(textures)
    }
}

#[derive(Debug, Serialize, Deserialize)]
pub enum Animation {
    Single(String),
    BlendSpace(BlendSpace),
    StateMashine(AnimationStateMashine),
    KeyFrames(KeyFrameAnimation),
}

impl Animation {
    pub fn get_all_textures(&self, textures: &mut HashSet<String>) {
        match self {
            Animation::Single(s) => {
                textures.insert(s.clone());
            }
            Animation::BlendSpace(i) => i.get_all_textures(textures),
            Animation::StateMashine(i) => i.get_all_textures(textures),
            Animation::KeyFrames(i) => i.get_all_textures(textures),
        }
    }
}

#[derive(Debug, Serialize, Deserialize)]
pub struct BlendSpace {
    pub points: Vec<BlendSpaceChild>,
    pub parameter: BlendSpaceParameter,
}

impl BlendSpace {
    pub fn get_all_textures(&self, textures: &mut HashSet<String>) {
        for point in &self.points {
            point.get_all_textures(textures);
        }
    }
}

#[derive(Debug, Serialize, Deserialize)]
pub enum BlendSpaceParameter {
    Vector(String),
    Values(String, String),
}

#[derive(Debug, Serialize, Deserialize)]
pub struct BlendSpaceChild {
    pub position: Position<f32>,
    pub child: Animation,
}

impl BlendSpaceChild {
    pub fn get_all_textures(&self, textures: &mut HashSet<String>) {
        self.child.get_all_textures(textures);
    }
}

#[derive(Debug, Serialize, Deserialize)]
pub struct AnimationStateMashine {
    pub start_node: Id,
    pub end_nodes: Vec<Id>,
    pub nodes: HashMap<Id, AnimationStateMashineNode>,
    pub edged: Vec<AnimationStateMashineEdge>,
}

impl AnimationStateMashine {
    pub fn get_all_textures(&self, textures: &mut HashSet<String>) {
        for (_, node) in &self.nodes {
            node.get_all_textures(textures);
        }
    }
}

#[derive(Debug, Serialize, Deserialize)]
pub struct AnimationStateMashineNode {
    pub name: String,
    pub child: Animation,
}

impl AnimationStateMashineNode {
    pub fn get_all_textures(&self, textures: &mut HashSet<String>) {
        self.child.get_all_textures(textures);
    }
}

#[derive(Debug, Serialize, Deserialize)]
pub struct AnimationStateMashineEdge {
    pub from: Id,
    pub to: Id,
    pub weight: u32,
    pub transition_type: TransitionType,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub enum TransitionType {
    Immediate,
    Finished,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct KeyFrameAnimation {
    pub frames: Vec<AnimationKeyFrame>,
    pub repeat: Repeat,
}

impl KeyFrameAnimation {
    pub fn get_all_textures(&self, textures: &mut HashSet<String>) {
        for frame in &self.frames {
            frame.get_all_textures(textures);
        }
    }
}

#[derive(Debug, Serialize, Deserialize)]
pub enum Repeat {
    None,
    Infinite,
    // Repeat(u32),
}

#[derive(Debug, Serialize, Deserialize)]
pub struct AnimationKeyFrame {
    pub duration: u64,
    pub frame: String,
}

impl AnimationKeyFrame {
    pub fn get_all_textures(&self, textures: &mut HashSet<String>) {
        textures.insert(self.frame.clone());
    }
}

#[derive(Debug, Serialize, Deserialize, Clone, Copy)]
pub struct Size<T> {
    pub width: T,
    pub height: T,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct Map {
    pub size: Size<NonZeroU32>,
    pub bg_tile: Option<String>,
    pub object_refs: HashMap<Id, ObjectRef>,
    pub actor_refs: HashMap<Id, ActorRef>,
}

impl Map {
    pub fn get_all_textures(&self, textures: &mut HashSet<String>) {
        if let Some(texture) = &self.bg_tile {
            textures.insert(texture.clone());
        }
    }
}

#[derive(Debug, Serialize, Deserialize)]
pub struct ObjectRef {
    pub object_id: Id,
    pub map_id: Id,
    pub position: Position<u32>,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct ActorRef {
    pub actor_id: Id,
    pub map_id: Id,
    pub position: Position<f32>,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct Position<T> {
    pub x: T,
    pub y: T,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct Object {
    pub collision: bool,
    pub texture: String,
}

impl Object {
    pub fn get_all_textures(&self, textures: &mut HashSet<String>) {
        textures.insert(self.texture.clone());
    }
}
