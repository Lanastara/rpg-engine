use std::{collections::HashMap, io::Write, num::NonZeroU32};

use rpg_world::data::*;

#[derive(Debug, Clone, Copy)]
pub enum Tile {
    Wall,
    Empty,
}

#[derive(Debug)]
pub struct SimpleMap {
    tiles: HashMap<usize, Tile>,
    pub width: i32,
    pub height: i32,
    background: Tile,
}

impl Default for SimpleMap {
    fn default() -> Self {
        Self::new()
    }
}

impl SimpleMap {
    pub fn new() -> Self {
        let sample = "................................................................\
		                   ................................................................\
		                   ................................................................\
		                   ................................................................\
		                   .......................########.................................\
		                   .....##########.......###..............#.#......................\
		                   ....................###................#.#......................\
		                   ...................####.........................................\
		                   ####################################.##############.....########\
		                   ...................................#.#...............###........\
		                   ........................############.#............###...........\
		                   ........................#............#.........###..............\
		                   ........................#.############......###.................\
		                   ........................#................###....................\
		                   ........................#################.......................\
		                   ................................................................";

        let tiles: HashMap<_, _> = sample
            .chars()
            .filter_map(|c| match c {
                '#' => Some(Tile::Wall),
                '.' => Some(Tile::Empty),
                _ => None,
            })
            .enumerate()
            .collect();

        Self {
            width: 64,
            height: 16,
            tiles,
            background: Tile::Wall,
        }
    }

    fn get_index(&self, x: i32, y: i32) -> Option<usize> {
        if x >= self.width || y >= self.height || x < 0 || y < 0 {
            None
        } else {
            Some((y * self.width + x) as usize)
        }
    }

    pub fn get_tile(&self, x: i32, y: i32) -> Option<Tile> {
        self.get_index(x, y).map_or(Some(self.background), |index| {
            self.tiles.get(&index).copied()
        })
    }
}

fn main() {
    let south = format!("map/player_s.png");
    let south_01 = format!("map/player_s_01.png");
    let south_02 = format!("map/player_s_02.png");
    let east = format!("map/player_e.png");
    let east_01 = format!("map/player_e_01.png");
    let east_02 = format!("map/player_e_02.png");
    let north = format!("map/player_n.png");
    let north_01 = format!("map/player_n_01.png");
    let north_02 = format!("map/player_n_02.png");
    let west = format!("map/player_w.png");
    let west_01 = format!("map/player_w_01.png");
    let west_02 = format!("map/player_w_02.png");

    let simple_map = SimpleMap::default();

    let mut index = u32::default();

    let player_id = index;
    index += 1;

    let map_id = index;
    index += 1;

    let player_ref_id = index;
    index += 1;

    let mut objects = HashMap::new();

    let wall_id = index;
    index += 1;

    objects.insert(
        wall_id,
        Object {
            collision: true,
            texture: "map/tree.png".to_string(),
        },
    );

    let mut actors = HashMap::new();

    let walking_south_animation = KeyFrameAnimation {
        frames: vec![
            AnimationKeyFrame {
                frame: south_01,
                duration: 200,
            },
            AnimationKeyFrame {
                frame: south.clone(),
                duration: 200,
            },
            AnimationKeyFrame {
                frame: south_02,
                duration: 200,
            },
            AnimationKeyFrame {
                frame: south.clone(),
                duration: 200,
            },
        ],
        repeat: Repeat::Infinite,
    };

    let walking_east_animation = KeyFrameAnimation {
        frames: vec![
            AnimationKeyFrame {
                frame: east_01,
                duration: 200,
            },
            AnimationKeyFrame {
                frame: east.clone(),
                duration: 200,
            },
            AnimationKeyFrame {
                frame: east_02,
                duration: 200,
            },
            AnimationKeyFrame {
                frame: east.clone(),
                duration: 200,
            },
        ],
        repeat: Repeat::Infinite,
    };

    let walking_north_animation = KeyFrameAnimation {
        frames: vec![
            AnimationKeyFrame {
                frame: north_01,
                duration: 200,
            },
            AnimationKeyFrame {
                frame: north.clone(),
                duration: 200,
            },
            AnimationKeyFrame {
                frame: north_02,
                duration: 200,
            },
            AnimationKeyFrame {
                frame: north.clone(),
                duration: 200,
            },
        ],
        repeat: Repeat::Infinite,
    };

    let walking_west_animation = KeyFrameAnimation {
        frames: vec![
            AnimationKeyFrame {
                frame: west_01,
                duration: 200,
            },
            AnimationKeyFrame {
                frame: west.clone(),
                duration: 200,
            },
            AnimationKeyFrame {
                frame: west_02,
                duration: 200,
            },
            AnimationKeyFrame {
                frame: west.clone(),
                duration: 200,
            },
        ],
        repeat: Repeat::Infinite,
    };

    let idle_blend_space_children = vec![
        BlendSpaceChild {
            child: Animation::Single(west),
            position: Position { x: -1.0, y: 0.0 },
        },
        BlendSpaceChild {
            child: Animation::Single(east),
            position: Position { x: 1.0, y: 0.0 },
        },
        BlendSpaceChild {
            child: Animation::Single(south),
            position: Position {
                x: 0.0,
                y: 0.999,
            },
        },
        BlendSpaceChild {
            child: Animation::Single(north),
            position: Position { x: 0.0, y: -1.0 },
        },
    ];

    let walking_blend_space_children = vec![
        BlendSpaceChild {
            child: Animation::KeyFrames(walking_west_animation),
            position: Position { x: -1.0, y: 0.0 },
        },
        BlendSpaceChild {
            child: Animation::KeyFrames(walking_east_animation),
            position: Position { x: 1.0, y: 0.0 },
        },
        BlendSpaceChild {
            child: Animation::KeyFrames(walking_south_animation),
            position: Position { x: 0.0, y: 1.0 },
        },
        BlendSpaceChild {
            child: Animation::KeyFrames(walking_north_animation),
            position: Position { x: 0.0, y: -1.0 },
        },
    ];

    let idle_blend_space = BlendSpace {
        parameter: BlendSpaceParameter::Vector("input".to_string()),
        points: idle_blend_space_children,
    };

    let walking_blend_space = BlendSpace {
        points: walking_blend_space_children,
        parameter: BlendSpaceParameter::Vector("input".to_string()),
    };

    let animation = AnimationStateMashine {
        nodes: vec![
            (
                0,
                AnimationStateMashineNode {
                    name: "idle".to_string(),
                    child: Animation::BlendSpace(idle_blend_space),
                },
            ),
            (
                1,
                AnimationStateMashineNode {
                    name: "walking".to_string(),
                    child: Animation::BlendSpace(walking_blend_space),
                },
            ),
        ]
        .into_iter()
        .collect(),
        start_node: 0,
        end_nodes: vec![],
        edged: vec![
            AnimationStateMashineEdge {
                from: 0,
                to: 1,
                weight: 1,
                transition_type: TransitionType::Immediate,
            },
            AnimationStateMashineEdge {
                from: 1,
                to: 0,
                weight: 1,
                transition_type: TransitionType::Immediate,
            },
        ],
    };

    actors.insert(
        player_id,
        Actor {
            animation: { Animation::StateMashine(animation) },
        },
    );

    let mut object_refs = HashMap::new();

    for y in 0..simple_map.height {
        for x in 0..simple_map.width {
            let tile = simple_map.get_tile(x, y);
            match tile {
                Some(Tile::Wall) => {
                    let id = index;
                    index += 1;

                    object_refs.insert(
                        id,
                        ObjectRef {
                            map_id,
                            object_id: wall_id,
                            position: Position {
                                x: x as u32,
                                y: y as u32,
                            },
                        },
                    );
                }
                _ => {}
            }
        }
    }

    let mut actor_refs = HashMap::new();
    actor_refs.insert(
        player_ref_id,
        ActorRef {
            actor_id: player_id,
            map_id,
            position: Position { x: 6.0, y: 6.0 },
        },
    );

    let map = rpg_world::data::Map {
        size: Size {
            width: NonZeroU32::new(simple_map.width as u32).unwrap(),
            height: NonZeroU32::new(simple_map.height as u32).unwrap(),
        },
        bg_tile: None,
        actor_refs,
        object_refs,
    };

    let mut maps = HashMap::new();
    maps.insert(map_id, map);

    let world = rpg_world::data::Master {
        globals: Globals {
            player: PlayerRef {
                map_id,
                refernce_id: player_ref_id,
            },
        },
        actors: actors,
        objects,
        maps,
    };

    let world_binary = bincode::serialize(&world).unwrap();

    let mut binary_file = std::fs::File::create("data/game.bwm").unwrap();
    binary_file.write_all(&world_binary[..]).unwrap();

    #[cfg(feature = "json")]
    {
        let json_file = std::fs::File::create("data/game.jwm").unwrap();
        serde_json::to_writer_pretty(json_file, &world).unwrap();
    }
}
