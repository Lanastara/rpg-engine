use bevy_ecs::{prelude::Query, system::ResMut};
use cgmath::{Vector2, Zero};
use image::DynamicImage;
use tracing::{instrument, warn};

use crate::{
    texture_atlases::{TextureAtlasHandle, TextureAtlases},
    Handle,
};

pub mod backend;
pub mod texture;

pub struct Sprite {
    pub handle: Option<Handle<DynamicImage>>,
}

pub struct AtlasTexture {
    pub position: cgmath::Vector2<f32>,
    pub scale: cgmath::Vector2<f32>,
}

impl Default for AtlasTexture {
    fn default() -> Self {
        Self {
            position: cgmath::Vector2::zero(),
            scale: cgmath::Vector2::zero(),
        }
    }
}

#[derive(Debug)]
pub struct ScreenPosition {
    pub position: cgmath::Vector2<f32>,
    pub scale: cgmath::Vector2<f32>,
}

#[derive(Debug, Default)]
pub struct Render(pub bool);

impl Default for ScreenPosition {
    fn default() -> Self {
        Self {
            position: Vector2::zero(),
            scale: Vector2::zero(),
        }
    }
}

#[instrument(skip(atlas_query, atlases))]
pub fn resolve_texture_atlas(
    mut atlas_query: Query<(&Sprite, &TextureAtlasHandle, &mut AtlasTexture)>,
    atlases: ResMut<TextureAtlases>,
) {
    for (sprite, atlas, mut texture) in atlas_query.iter_mut() {
        if let Some(atlas) = atlases.get(&atlas) {
            if let Some(handle) = sprite.handle {
                if let Some(pos) = atlas.get_loaded_texture(handle) {
                    *texture = AtlasTexture {
                        position: Vector2::new(pos.x as f32, pos.y as f32),
                        scale: Vector2::new(pos.width as f32, pos.height as f32),
                    };
                }
            }
        } else {
            warn!("Texture atlas not found");
        }
    }
}
