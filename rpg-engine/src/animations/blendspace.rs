use cgmath::{InnerSpace, Vector2, Zero};

use super::{Animate, Animation, AnimationFrame};

#[derive(Debug, Clone)]
pub struct BlendSpace2 {
    child_map: Vec<(Vector2<f32>, usize)>,
    children: Vec<Animation>,
    parameter: BlendSpace2Parameter,
    current: usize,
}

#[derive(Debug, Clone)]
pub enum BlendSpace2Parameter {
    Vector(String),
    Values(String, String),
}

impl BlendSpace2 {
    pub fn new(children: Vec<BlendSpace2Child>, parameter: BlendSpace2Parameter) -> Self {
        let mut child_map = Vec::with_capacity(children.len());
        let mut inner_children = Vec::with_capacity(children.len());

        for (
            index,
            BlendSpace2Child {
                position,
                animation,
            },
        ) in children.into_iter().enumerate()
        {
            inner_children.push(animation);
            child_map.push((position, index));
        }

        Self {
            children: inner_children,
            parameter,
            child_map,
            current: 0,
        }
    }
}

impl Animate for BlendSpace2 {
    fn get_animation_frame(
        &mut self,
        delta_t: std::time::Duration,
        parameters: &super::AnimationParameters,
    ) -> Option<AnimationFrame> {
        let position = match self.parameter {
            BlendSpace2Parameter::Vector(ref name) => {
                parameters.get_vector(&name).unwrap_or_else(Zero::zero)
            }
            BlendSpace2Parameter::Values(ref name_x, ref name_y) => Vector2::new(
                parameters.get_float(&name_x).unwrap_or_default(),
                parameters.get_float(&name_y).unwrap_or_default(),
            ),
        };

        let (_disdance, (_position, index)) = self
            .child_map
            .iter_mut()
            .map(|c| ((c.0 - position).magnitude2(), c))
            .min_by(|c1, c2| c1.0.partial_cmp(&c2.0).unwrap())?;

        let animation = self.children.get_mut(*index)?;

        // TODO: implement animations resetting when switching

        animation.get_animation_frame(delta_t, parameters)
    }

    fn travel(&mut self, _target: &str) {
        todo!();
    }
}

#[derive(Debug, Clone)]
pub struct BlendSpace2Child {
    pub position: Vector2<f32>,
    pub animation: Animation,
}
