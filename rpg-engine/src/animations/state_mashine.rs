use std::collections::HashMap;

use petgraph::{graph::NodeIndex, Directed, Graph};

use super::{Animate, Animation, AnimationFrame};

#[derive(Debug, Clone)]
pub struct Transition {
    pub priority: u32,
    pub transition_type: TransitionType,
}

#[derive(Debug, Clone)]
pub enum TransitionType {
    Immediate,
    Finished,
}

#[derive(Debug, Clone)]
pub struct StateMashine {
    graph: Graph<Animation, Transition, Directed>,
    current_node: NodeIndex,
    names: HashMap<String, NodeIndex>,
    start_node: NodeIndex,
    end_nodes: Vec<NodeIndex>,
    target: Option<String>,
}

impl StateMashine {
    pub fn builder() -> StateMashineBuilder {
        StateMashineBuilder::default()
    }

    fn travel_towards(
        &mut self,
        target: NodeIndex,
        _child_target: Option<&str>,
        inner_looped: bool,
    ) {
        let path = petgraph::algo::astar(
            &self.graph,
            self.current_node,
            |n| n == target,
            |t| t.weight().priority,
            |_| 0,
        );

        if let Some((_, path)) = path {
            let mut from = None;
            for to in path {
                if let Some(from) = from {
                    let edge = self.graph.find_edge(from, to).unwrap();
                    let transition = self.graph.edge_weight(edge).unwrap();
                    match transition.transition_type {
                        TransitionType::Immediate => {
                            self.current_node = to;
                        }
                        TransitionType::Finished if inner_looped => {
                            self.current_node = to;
                        }
                        TransitionType::Finished => {
                            return;
                        }
                    }
                }

                from = Some(to);
            }
        }
    }
}

impl Animate for StateMashine {
    fn get_animation_frame(
        &mut self,
        delta_t: std::time::Duration,
        parameters: &super::AnimationParameters,
    ) -> Option<AnimationFrame> {
        let node = self.graph.node_weight_mut(self.current_node)?;

        node.get_animation_frame(delta_t, parameters)
    }

    fn travel(&mut self, target: &str) {
        if let Some((target, child_target)) = target.split_once('/') {
            if let Some(&index) = self.names.get(target) {
                self.travel_towards(index, Some(child_target), false);
            }
        } else {
            if let Some(&index) = self.names.get(target) {
                self.travel_towards(index, None, false);
            }
        }
    }
}

#[derive(Default)]
pub struct StateMashineBuilder {
    graph: Graph<Animation, Transition, Directed>,
    names: HashMap<String, NodeIndex>,
    start_node: Option<NodeIndex>,
    end_nodes: Vec<NodeIndex>,
}

impl StateMashineBuilder {
    pub fn add_end_node(&mut self, animation: Animation, name: String) -> NodeIndex {
        let index = self.add_node(animation, name);

        self.end_nodes.push(index);

        index
    }

    pub fn add_start_node(&mut self, animation: Animation, name: String) -> NodeIndex {
        let index = self.add_node(animation, name);

        self.start_node = Some(index);

        index
    }

    pub fn add_node(&mut self, animation: Animation, name: String) -> NodeIndex {
        let index = self.graph.add_node(animation);
        self.names.insert(name, index);

        index
    }

    pub fn add_transition(&mut self, from: NodeIndex, to: NodeIndex, transition: Transition) {
        self.graph.add_edge(from, to, transition);
    }

    pub fn build(self) -> Option<StateMashine> {
        let start_node = self.start_node?;

        Some(StateMashine {
            graph: self.graph,
            current_node: start_node,
            names: self.names,
            start_node,
            end_nodes: self.end_nodes,
            target: None,
        })
    }
}
