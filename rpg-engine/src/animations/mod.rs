use std::{collections::HashMap, time::Duration};

use bevy_ecs::prelude::{Query, Res};
use cgmath::Vector2;
use image::DynamicImage;
use tracing::warn;

use crate::{graphics::Sprite, Handle};

mod blendspace;
mod key_frame;
mod state_mashine;

pub use blendspace::*;
pub use key_frame::*;
pub use state_mashine::*;

struct AnimationFrame {
    frame: Handle<DynamicImage>,
    _first_frame: bool,
}

trait Animate {
    fn get_animation_frame(
        &mut self,
        delta_t: Duration,
        _parameters: &AnimationParameters,
    ) -> Option<AnimationFrame>;

    fn travel(&mut self, target: &str);
}

impl Animate for Handle<DynamicImage> {
    fn get_animation_frame(
        &mut self,
        _delta_t: Duration,
        _parameters: &AnimationParameters,
    ) -> Option<AnimationFrame> {
        Some(AnimationFrame {
            frame: *self,
            _first_frame: true,
        })
    }

    fn travel(&mut self, _target: &str) {}
}

pub fn animation(
    mut animations: Query<(&mut Animation, &mut Sprite, &AnimationParameters)>,
    delta_t: Res<Duration>,
) {
    for (mut anim, mut draw, parameters) in animations.iter_mut() {
        let target = parameters.get_string("target");

        if let Some(target) = target {
            anim.travel(&target);
        }

        let frame_handle = anim.get_animation_frame(*delta_t, parameters);

        if let Some(frame_handle) = frame_handle {
            draw.handle.replace(frame_handle.frame);
        } else {
            warn!(?parameters, "no animation frame");
        }
    }
}

#[derive(Debug, Clone)]
pub enum Animation {
    Frame(Handle<DynamicImage>),
    KeyFrameAnimation(KeyFrameAnimation),
    BlendSpace2(BlendSpace2),
    StateMashine(StateMashine),
}

impl Animate for Animation {
    fn get_animation_frame(
        &mut self,
        delta_t: Duration,
        parameters: &AnimationParameters,
    ) -> Option<AnimationFrame> {
        match self {
            Animation::KeyFrameAnimation(a) => a.get_animation_frame(delta_t, parameters),
            Animation::BlendSpace2(b) => b.get_animation_frame(delta_t, parameters),
            Animation::Frame(f) => f.get_animation_frame(delta_t, parameters),
            Animation::StateMashine(s) => s.get_animation_frame(delta_t, parameters),
        }
    }

    fn travel(&mut self, target: &str) {
        match self {
            Animation::KeyFrameAnimation(a) => a.travel(target),
            Animation::BlendSpace2(b) => b.travel(target),
            Animation::Frame(f) => f.travel(target),
            Animation::StateMashine(s) => s.travel(target),
        }
    }
}

#[derive(Debug, Default)]
pub struct AnimationParameters {
    data: HashMap<String, AnimationParameter>,
}

impl AnimationParameters {
    pub fn get_float(&self, name: &str) -> Option<f32> {
        self.get_value(name)
            .map(AnimationParameter::get_float)
            .flatten()
    }

    pub fn get_vector(&self, name: &str) -> Option<Vector2<f32>> {
        self.get_value(name)
            .map(AnimationParameter::get_vector)
            .flatten()
    }

    pub fn get_string(&self, name: &str) -> Option<String> {
        self.get_value(name)
            .map(AnimationParameter::get_string)
            .flatten()
    }

    pub fn set_vector(&mut self, name: String, value: Vector2<f32>) {
        self.set_value(name, AnimationParameter::Vector(value))
    }

    pub fn set_string(&mut self, name: String, value: String) {
        self.set_value(name, AnimationParameter::String(value))
    }

    pub fn get_value(&self, name: &str) -> Option<AnimationParameter> {
        self.data.get(name).cloned()
    }

    pub fn set_value(&mut self, name: String, value: AnimationParameter) {
        self.data.insert(name, value);
    }
}

#[derive(Debug, Clone)]
pub enum AnimationParameter {
    Float(f32),
    String(String),
    Int(i32),
    Object(Box<AnimationParameter>),
    Vector(Vector2<f32>),
}

impl AnimationParameter {
    fn get_float(self) -> Option<f32> {
        match self {
            AnimationParameter::Float(f) => Some(f),
            _ => None,
        }
    }

    fn get_vector(self) -> Option<Vector2<f32>> {
        match self {
            AnimationParameter::Vector(v) => Some(v),
            _ => None,
        }
    }

    fn get_string(self) -> Option<String> {
        match self {
            AnimationParameter::String(v) => Some(v),
            _ => None,
        }
    }
}
