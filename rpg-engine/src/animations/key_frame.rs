use std::time::Duration;

use image::DynamicImage;

use crate::Handle;

use super::{Animate, AnimationFrame, AnimationParameters};

#[derive(Debug, Clone)]
pub enum Repeat {
    None,
    Infinite,
    // Repeat(u32),
}

#[derive(Debug, Clone)]
pub struct KeyFrameAnimation {
    duration: Duration,
    iteration_duration: Duration,
    frames: Vec<KeyFrame>,
    repeat: Repeat,
}

impl Animate for KeyFrameAnimation {
    fn get_animation_frame(
        &mut self,
        delta_t: Duration,
        _parameters: &AnimationParameters,
    ) -> Option<AnimationFrame> {
        self.duration += delta_t;

        let iteration = self.duration.as_micros() / self.iteration_duration.as_micros();
        let remainder = self.duration.as_micros() % self.iteration_duration.as_micros();

        match self.repeat {
            Repeat::None => {
                if iteration > 0 {
                    return None;
                }
            }
            _ => {}
        }

        let mut t_accum = 0;
        let mut first_frame = true;

        for frame in &self.frames {
            t_accum += frame.duration.as_micros();

            if t_accum > remainder {
                return Some(AnimationFrame {
                    frame: frame.frame.clone(),
                    _first_frame : first_frame,
                });
            }

            first_frame = false;
        }

        None
    }

    fn travel(&mut self, _target: &str) {}
}

impl KeyFrameAnimation {
    pub fn new(frames: Vec<KeyFrame>, repeat: Repeat) -> Self {
        let iteration_duration = frames.iter().map(|f| f.duration).sum();
        Self {
            frames,
            repeat,
            iteration_duration,
            duration: Duration::default(),
        }
    }
}

#[derive(Debug, Clone)]
pub struct KeyFrame {
    pub frame: Handle<DynamicImage>,
    pub duration: Duration,
}
