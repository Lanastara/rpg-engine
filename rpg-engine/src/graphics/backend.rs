use std::collections::HashMap;

use bevy_ecs::{
    prelude::Query,
    system::{Res, ResMut},
};
use cgmath::Matrix4;
use image::ImageResult;
use tracing::{instrument, warn};
use wgpu::{
    util::{BufferInitDescriptor, DeviceExt},
    BindGroupLayout, SwapChainError, SwapChainTexture,
};
use winit::{dpi::PhysicalSize, window::Window};

use crate::{
    assets::{Assets, LoadState},
    texture_atlases::TextureAtlasHandle,
};
use crate::{texture_atlases::TextureAtlases, WindowSize};

use super::{texture::Texture, AtlasTexture, Render, ScreenPosition};

// impl Draw {
//     fn to_raw(&self) -> (PositionRaw, ScaleRaw) {
//         (
//             PositionRaw {
//                 model: self.position.into(),
//             },
//             ScaleRaw {
//                 model: self.scale.into(),
//             },
//         )
//     }

//     fn to_texture_raw(&self) -> (TexturePositionRaw, TextureSizeRaw) {
//         (
//             TexturePositionRaw {
//                 model: self.uv_position.into(),
//             },
//             TextureSizeRaw {
//                 model: self.uv_size.into(),
//             },
//         )
//     }
// }

impl AtlasTexture {
    fn to_raw(&self) -> (TexturePositionRaw, TextureSizeRaw) {
        (
            TexturePositionRaw {
                model: self.position.into(),
            },
            TextureSizeRaw {
                model: self.scale.into(),
            },
        )
    }
}

impl ScreenPosition {
    fn to_raw(&self) -> (PositionRaw, ScaleRaw) {
        (
            PositionRaw {
                model: self.position.into(),
            },
            ScaleRaw {
                model: self.scale.into(),
            },
        )
    }
}

#[repr(C)]
#[derive(Copy, Clone, bytemuck::Pod, bytemuck::Zeroable, Default)]
struct PositionRaw {
    model: [f32; 2],
}

impl PositionRaw {
    fn desc<'a>() -> wgpu::VertexBufferLayout<'a> {
        use std::mem;
        wgpu::VertexBufferLayout {
            array_stride: mem::size_of::<PositionRaw>() as wgpu::BufferAddress,
            step_mode: wgpu::InputStepMode::Instance,
            attributes: &[wgpu::VertexAttribute {
                offset: 0,
                shader_location: 2,
                format: wgpu::VertexFormat::Float2,
            }],
        }
    }
}

#[repr(C)]
#[derive(Copy, Clone, bytemuck::Pod, bytemuck::Zeroable, Default)]
struct ScaleRaw {
    model: [f32; 2],
}

impl ScaleRaw {
    fn desc<'a>() -> wgpu::VertexBufferLayout<'a> {
        use std::mem;
        wgpu::VertexBufferLayout {
            array_stride: mem::size_of::<ScaleRaw>() as wgpu::BufferAddress,
            // We need to switch from using a step mode of Vertex to Instance
            // This means that our shaders will only change to use the next
            // instance when the shader starts processing a new instance
            step_mode: wgpu::InputStepMode::Instance,
            attributes: &[wgpu::VertexAttribute {
                offset: 0,
                shader_location: 3,
                format: wgpu::VertexFormat::Float2,
            }],
        }
    }
}

#[repr(C)]
#[derive(Copy, Clone, bytemuck::Pod, bytemuck::Zeroable, Default)]
struct TexturePositionRaw {
    model: [f32; 2],
}

impl TexturePositionRaw {
    fn desc<'a>() -> wgpu::VertexBufferLayout<'a> {
        use std::mem;
        wgpu::VertexBufferLayout {
            array_stride: mem::size_of::<PositionRaw>() as wgpu::BufferAddress,
            step_mode: wgpu::InputStepMode::Instance,
            attributes: &[wgpu::VertexAttribute {
                offset: 0,
                shader_location: 4,
                format: wgpu::VertexFormat::Float2,
            }],
        }
    }
}

#[repr(C)]
#[derive(Copy, Clone, bytemuck::Pod, bytemuck::Zeroable, Default)]
struct TextureSizeRaw {
    model: [f32; 2],
}

impl TextureSizeRaw {
    fn desc<'a>() -> wgpu::VertexBufferLayout<'a> {
        use std::mem;
        wgpu::VertexBufferLayout {
            array_stride: mem::size_of::<ScaleRaw>() as wgpu::BufferAddress,
            // We need to switch from using a step mode of Vertex to Instance
            // This means that our shaders will only change to use the next
            // instance when the shader starts processing a new instance
            step_mode: wgpu::InputStepMode::Instance,
            attributes: &[wgpu::VertexAttribute {
                offset: 0,
                shader_location: 5,
                format: wgpu::VertexFormat::Float2,
            }],
        }
    }
}

#[repr(C)]
#[derive(Copy, Clone, Debug, bytemuck::Pod, bytemuck::Zeroable)]
struct Vertex {
    position: [f32; 3],
    uv: [f32; 2],
}

impl Vertex {
    fn desc<'a>() -> wgpu::VertexBufferLayout<'a> {
        wgpu::VertexBufferLayout {
            array_stride: std::mem::size_of::<Vertex>() as wgpu::BufferAddress,
            step_mode: wgpu::InputStepMode::Vertex,
            attributes: &[
                wgpu::VertexAttribute {
                    offset: 0,
                    shader_location: 0,
                    format: wgpu::VertexFormat::Float3,
                },
                wgpu::VertexAttribute {
                    offset: std::mem::size_of::<[f32; 3]>() as wgpu::BufferAddress,
                    shader_location: 1,
                    format: wgpu::VertexFormat::Float2,
                },
            ],
        }
    }
}

const VERTICES: &[Vertex] = &[
    Vertex {
        position: [1.0, 1.0, 0.0],
        uv: [1.0, 1.0],
    },
    Vertex {
        position: [1.0, 0.0, 0.0],
        uv: [1.0, 0.0],
    },
    Vertex {
        position: [0.0, 1.0, 0.0],
        uv: [0.0, 1.0],
    },
    Vertex {
        position: [0.0, 0.0, 0.0],
        uv: [0.0, 0.0],
    },
];

const INDICES: &[u16] = &[0, 2, 1, 1, 2, 3];

#[repr(C)]
// This is so we can store this in a buffer
#[derive(Debug, Copy, Clone, bytemuck::Pod, bytemuck::Zeroable)]
struct Uniforms {
    // We can't use cgmath with bytemuck directly so we'll have
    // to convert the Matrix4 into a 4x4 f32 array
    view_proj: [[f32; 4]; 4],
}

impl Uniforms {
    fn new() -> Self {
        use cgmath::SquareMatrix;
        Self {
            view_proj: cgmath::Matrix4::identity().into(),
        }
    }

    fn update_view_proj(&mut self, translation: Matrix4<f32>) {
        self.view_proj = translation.into();
    }
}

pub struct Graphics {
    surface: wgpu::Surface,
    pub device: wgpu::Device,
    pub queue: wgpu::Queue,
    sc_desc: wgpu::SwapChainDescriptor,
    swap_chain: wgpu::SwapChain,
    size: winit::dpi::PhysicalSize<u32>,
    render_pipeline: wgpu::RenderPipeline,
    vertex_buffer: wgpu::Buffer,
    index_buffer: wgpu::Buffer,
    num_indices: u32,
    uniforms: Uniforms,
    uniform_bind_group: wgpu::BindGroup,
    uniform_buffer: wgpu::Buffer,
    position_buffer: wgpu::Buffer,
    scale_buffer: wgpu::Buffer,
    texture_position_buffer: wgpu::Buffer,
    texture_size_buffer: wgpu::Buffer,
    sampler: wgpu::Sampler,
    texture_bind_group_layout: BindGroupLayout,
}

impl Graphics {
    pub async fn new(window: &Window) -> ImageResult<Self> {
        let size = window.inner_size();

        // The instance is a handle to our GPU
        // BackendBit::PRIMARY => Vulkan + Metal + DX12 + Browser WebGPU
        let instance = wgpu::Instance::new(wgpu::BackendBit::PRIMARY);
        let surface = unsafe { instance.create_surface(window) };
        let adapter = instance
            .request_adapter(&wgpu::RequestAdapterOptions {
                power_preference: wgpu::PowerPreference::default(),
                compatible_surface: Some(&surface),
            })
            .await
            .unwrap();

        let (device, queue) = adapter
            .request_device(
                &wgpu::DeviceDescriptor {
                    features: wgpu::Features::empty(),
                    limits: wgpu::Limits::default(),
                    label: None,
                },
                None, // Trace path
            )
            .await
            .unwrap();

        let sc_desc = wgpu::SwapChainDescriptor {
            usage: wgpu::TextureUsage::RENDER_ATTACHMENT,
            format: adapter.get_swap_chain_preferred_format(&surface),
            width: size.width,
            height: size.height,
            present_mode: wgpu::PresentMode::Fifo,
        };
        let swap_chain = device.create_swap_chain(&surface, &sc_desc);

        let texture_bind_group_layout =
            device.create_bind_group_layout(&wgpu::BindGroupLayoutDescriptor {
                entries: &[
                    wgpu::BindGroupLayoutEntry {
                        binding: 0,
                        visibility: wgpu::ShaderStage::FRAGMENT,
                        ty: wgpu::BindingType::Texture {
                            multisampled: false,
                            view_dimension: wgpu::TextureViewDimension::D2Array,
                            sample_type: wgpu::TextureSampleType::Float { filterable: false },
                        },
                        count: None,
                    },
                    wgpu::BindGroupLayoutEntry {
                        binding: 1,
                        visibility: wgpu::ShaderStage::FRAGMENT,
                        ty: wgpu::BindingType::Sampler {
                            comparison: false,
                            filtering: false,
                        },
                        count: None,
                    },
                ],
                label: Some("texture_bind_group_layout"),
            });

        let sampler = device.create_sampler(&wgpu::SamplerDescriptor {
            address_mode_u: wgpu::AddressMode::ClampToEdge,
            address_mode_v: wgpu::AddressMode::ClampToEdge,
            address_mode_w: wgpu::AddressMode::ClampToEdge,
            mag_filter: wgpu::FilterMode::Nearest,
            min_filter: wgpu::FilterMode::Nearest,
            mipmap_filter: wgpu::FilterMode::Nearest,
            ..Default::default()
        });

        let mut uniforms = Uniforms::new();
        uniforms.update_view_proj(Matrix4::new(
            2.0 / size.width as f32,
            0.0,
            0.0,
            0.0, //
            0.0,
            -2.0 / size.height as f32,
            0.0,
            0.0, //
            0.0,
            0.0,
            0.1,
            0.0, //
            -1.0,
            1.0,
            0.0,
            1.0, //
        ));

        let uniform_buffer = device.create_buffer_init(&wgpu::util::BufferInitDescriptor {
            label: Some("Uniform Buffer"),
            contents: bytemuck::cast_slice(&[uniforms]),
            usage: wgpu::BufferUsage::UNIFORM | wgpu::BufferUsage::COPY_DST,
        });

        let uniform_bind_group_layout =
            device.create_bind_group_layout(&wgpu::BindGroupLayoutDescriptor {
                entries: &[wgpu::BindGroupLayoutEntry {
                    binding: 0,
                    visibility: wgpu::ShaderStage::VERTEX,
                    ty: wgpu::BindingType::Buffer {
                        ty: wgpu::BufferBindingType::Uniform,
                        has_dynamic_offset: false,
                        min_binding_size: None,
                    },
                    count: None,
                }],
                label: Some("uniform_bind_group_layout"),
            });

        let uniform_bind_group = device.create_bind_group(&wgpu::BindGroupDescriptor {
            layout: &uniform_bind_group_layout,
            entries: &[wgpu::BindGroupEntry {
                binding: 0,
                resource: uniform_buffer.as_entire_binding(),
            }],
            label: Some("uniform_bind_group"),
        });

        let vs_module = device.create_shader_module(&wgpu::include_spirv!("shader.vert.spv"));
        let fs_module = device.create_shader_module(&wgpu::include_spirv!("shader.frag.spv"));

        let render_pipeline_layout =
            device.create_pipeline_layout(&wgpu::PipelineLayoutDescriptor {
                label: Some("Render Pipeline Layout"),
                bind_group_layouts: &[&texture_bind_group_layout, &uniform_bind_group_layout],
                push_constant_ranges: &[],
            });

        let render_pipeline = device.create_render_pipeline(&wgpu::RenderPipelineDescriptor {
            label: Some("Render Pipeline"),
            layout: Some(&render_pipeline_layout),
            vertex: wgpu::VertexState {
                module: &vs_module,
                entry_point: "main",
                buffers: &[
                    Vertex::desc(),
                    PositionRaw::desc(),
                    ScaleRaw::desc(),
                    TexturePositionRaw::desc(),
                    TextureSizeRaw::desc(),
                ],
            },
            fragment: Some(wgpu::FragmentState {
                module: &fs_module,
                entry_point: "main",
                targets: &[wgpu::ColorTargetState {
                    format: sc_desc.format,
                    alpha_blend: wgpu::BlendState::REPLACE,
                    color_blend: wgpu::BlendState::REPLACE,
                    write_mask: wgpu::ColorWrite::ALL,
                }],
            }),
            primitive: wgpu::PrimitiveState {
                topology: wgpu::PrimitiveTopology::TriangleList,
                strip_index_format: None,
                front_face: wgpu::FrontFace::Cw,
                cull_mode: wgpu::CullMode::Back,
                polygon_mode: wgpu::PolygonMode::Fill,
            },
            depth_stencil: None,
            multisample: wgpu::MultisampleState {
                count: 1,
                mask: !0,
                alpha_to_coverage_enabled: false,
            },
        });

        let vertex_buffer = device.create_buffer_init(&BufferInitDescriptor {
            label: Some("Vertex Buffer"),
            contents: bytemuck::cast_slice(VERTICES),
            usage: wgpu::BufferUsage::VERTEX,
        });

        let index_buffer = device.create_buffer_init(&BufferInitDescriptor {
            label: Some("Index Buffer"),
            contents: bytemuck::cast_slice(INDICES),
            usage: wgpu::BufferUsage::INDEX,
        });
        let num_indices = INDICES.len() as u32;

        let position_buffer = device.create_buffer(&wgpu::BufferDescriptor {
            label: Some("Position Buffer"),
            size: 100 * 1000 * 8,
            usage: wgpu::BufferUsage::VERTEX | wgpu::BufferUsage::COPY_DST,
            mapped_at_creation: false,
        });

        let scale_buffer = device.create_buffer(&wgpu::BufferDescriptor {
            label: Some("Scale Buffer"),
            size: 100 * 1000 * 8,
            usage: wgpu::BufferUsage::VERTEX | wgpu::BufferUsage::COPY_DST,
            mapped_at_creation: false,
        });

        let texture_position_buffer = device.create_buffer(&wgpu::BufferDescriptor {
            label: Some("Texture Position Buffer"),
            size: 100 * 1000 * 8,
            usage: wgpu::BufferUsage::VERTEX | wgpu::BufferUsage::COPY_DST,
            mapped_at_creation: false,
        });

        let texture_size_buffer = device.create_buffer(&wgpu::BufferDescriptor {
            label: Some("Texture size Buffer"),
            size: 100 * 1000 * 8,
            usage: wgpu::BufferUsage::VERTEX | wgpu::BufferUsage::COPY_DST,
            mapped_at_creation: false,
        });

        Ok(Self {
            surface,
            device,
            queue,
            sc_desc,
            swap_chain,
            size,
            render_pipeline,
            vertex_buffer,
            index_buffer,
            num_indices,
            sampler,
            uniforms,
            uniform_bind_group,
            uniform_buffer,
            position_buffer,
            scale_buffer,
            texture_position_buffer,
            texture_size_buffer,
            texture_bind_group_layout,
        })
    }

    fn resize(&mut self, new_size: PhysicalSize<u32>, force_resize: bool) {
        if self.size != new_size || force_resize {
            self.size = new_size;
            self.sc_desc.width = new_size.width;
            self.sc_desc.height = new_size.height;
            self.swap_chain = self.device.create_swap_chain(&self.surface, &self.sc_desc);
            self.uniforms.update_view_proj(Matrix4::new(
                2.0 / new_size.width as f32,
                0.0,
                0.0,
                0.0, //
                0.0,
                -2.0 / new_size.height as f32,
                0.0,
                0.0, //
                0.0,
                0.0,
                0.1,
                0.0, //
                -1.0,
                1.0,
                0.0,
                1.0, //
            ));

            self.queue.write_buffer(
                &self.uniform_buffer,
                0,
                bytemuck::cast_slice(&[self.uniforms]),
            );
        }
    }

    fn get_current_frame(&self) -> Result<SwapChainTexture, SwapChainError> {
        Ok(self.swap_chain.get_current_frame()?.output)
    }

    fn recreate_swapchain(&mut self) {
        self.resize(self.size, true);
    }

    fn render(
        &self,
        texture: &Texture,
        draws: Vec<(&AtlasTexture, &ScreenPosition)>,
    ) -> Result<(), wgpu::SwapChainError> {
        let frame = self.get_current_frame()?;
        let mut encoder = self
            .device
            .create_command_encoder(&wgpu::CommandEncoderDescriptor {
                label: Some("Render Encoder"),
            });

        let mut position_data = Vec::new();
        let mut scale_data = Vec::new();
        let mut texture_position_data = Vec::new();
        let mut texture_scale_data = Vec::new();

        let mut len = 0;

        for (atlas, screen) in draws.iter() {
            let (pos, scale) = screen.to_raw();
            position_data.push(pos);
            scale_data.push(scale);

            let (pos, scale) = atlas.to_raw();
            texture_position_data.push(pos);
            texture_scale_data.push(scale);

            len += 1;
        }

        // debug!(len, "rendering targets");

        self.queue.write_buffer(
            &self.position_buffer,
            0,
            bytemuck::cast_slice(&position_data),
        );

        self.queue
            .write_buffer(&self.scale_buffer, 0, bytemuck::cast_slice(&scale_data));

        self.queue.write_buffer(
            &self.texture_position_buffer,
            0,
            bytemuck::cast_slice(&texture_position_data),
        );

        self.queue.write_buffer(
            &self.texture_size_buffer,
            0,
            bytemuck::cast_slice(&texture_scale_data),
        );

        //info!(len, "rendering");

        let diffuse_bind_group = self.device.create_bind_group(&wgpu::BindGroupDescriptor {
            layout: &self.texture_bind_group_layout,
            entries: &[
                wgpu::BindGroupEntry {
                    binding: 0,
                    resource: wgpu::BindingResource::TextureView(&texture.view),
                },
                wgpu::BindGroupEntry {
                    binding: 1,
                    resource: wgpu::BindingResource::Sampler(&self.sampler),
                },
            ],
            label: Some("diffuse_bind_group"),
        });

        {
            let mut render_pass = encoder.begin_render_pass(&wgpu::RenderPassDescriptor {
                label: Some("Render Pass"),
                color_attachments: &[wgpu::RenderPassColorAttachmentDescriptor {
                    attachment: &frame.view,
                    resolve_target: None,
                    ops: wgpu::Operations {
                        load: wgpu::LoadOp::Clear(wgpu::Color {
                            r: 0.1,
                            g: 0.2,
                            b: 0.3,
                            a: 1.0,
                        }),
                        store: true,
                    },
                }],
                depth_stencil_attachment: None,
            });

            render_pass.set_pipeline(&self.render_pipeline);
            render_pass.set_bind_group(0, &diffuse_bind_group, &[]);
            render_pass.set_bind_group(1, &self.uniform_bind_group, &[]);

            render_pass.set_vertex_buffer(0, self.vertex_buffer.slice(..));
            render_pass.set_vertex_buffer(1, self.position_buffer.slice(..));
            render_pass.set_vertex_buffer(2, self.scale_buffer.slice(..));
            render_pass.set_vertex_buffer(3, self.texture_position_buffer.slice(..));
            render_pass.set_vertex_buffer(4, self.texture_size_buffer.slice(..));
            render_pass.set_index_buffer(self.index_buffer.slice(..), wgpu::IndexFormat::Uint16); // 1.
            render_pass.draw_indexed(0..self.num_indices, 0, 0..len as _);
        }

        // submit will accept anything that implements IntoIter
        self.queue.submit(std::iter::once(encoder.finish()));

        Ok(())
    }
}

#[instrument(skip(graphics, draws, atlases, assets))]
pub fn render(
    draws: Query<(&AtlasTexture, &ScreenPosition, &TextureAtlasHandle, &Render)>,
    atlases: Res<TextureAtlases>,
    mut graphics: ResMut<Graphics>,
    assets: Res<Assets>,
) {
    let mut pass_data: HashMap<TextureAtlasHandle, Vec<(&AtlasTexture, &ScreenPosition)>> =
        HashMap::new();

    for (texture, screen, atlas, render) in draws.iter() {
        if render.0 {
            match pass_data.get_mut(atlas) {
                Some(pass) => pass.push((texture, screen)),
                None => {
                    pass_data.insert(*atlas, vec![(texture, screen)]);
                }
            }
        }
    }

    for (handle, textures) in pass_data {
        if let Some(atlas) = atlases.get(&handle) {
            let texture = atlas.get_texture();
            let texture = assets.get(&texture);
            if let Some(texture) = texture {
                match *texture {
                    LoadState::Loading => {}
                    LoadState::Error => {}
                    LoadState::Loaded(ref texture) => {
                        match graphics.render(texture, textures) {
                            Ok(_) => {}
                            // Recreate the swap_chain if lost
                            Err(wgpu::SwapChainError::Lost) => {
                                graphics.recreate_swapchain();
                            }
                            // The system is out of memory, we should probably quit
                            Err(wgpu::SwapChainError::OutOfMemory) => {
                                panic!("Out of memory");
                            }
                            // All other errors (Outdated, Timeout) should be resolved by the next frame
                            Err(e) => eprintln!("{:?}", e),
                        }
                    }
                }
            }
        } else {
            warn!("texture atlas not found");
        }
    }
}

#[instrument(skip(graphics, window_size))]
pub fn update_size(mut graphics: ResMut<Graphics>, window_size: Res<WindowSize>) {
    graphics.resize(
        PhysicalSize::new(window_size.width, window_size.height),
        false,
    );
}
