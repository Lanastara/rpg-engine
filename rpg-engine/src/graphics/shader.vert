#version 450

layout(location=0) in vec3 a_position;

layout(location=1) in vec2 a_tex_coords;

layout(location=0) out vec2 v_tex_coords;
layout(location=1) out vec2 v_texture_position;
layout(location=2) out vec2 v_texture_size;

layout(set=1, binding=0) 
uniform Uniforms {
    mat4 u_view_proj; 
};

layout(location=2) in vec2 v_position;
layout(location=3) in vec2 v_scale;
layout(location=4) in vec2 a_texture_position;
layout(location=5) in vec2 a_texture_size;

void main() {
    mat4 model_matrix = mat4(
        v_scale.x, 0.0, 0.0, 0.0,
        0.0, v_scale.y, 0.0, 0.0,
        0.0, 0.0, 1, 0.0,
        v_position.x, v_position.y, 1.0, 1.0
    );
    v_tex_coords = a_tex_coords;
    v_texture_position = a_texture_position;
    v_texture_size = a_texture_size;
    gl_Position = u_view_proj * model_matrix * vec4(a_position, 1.0);
}