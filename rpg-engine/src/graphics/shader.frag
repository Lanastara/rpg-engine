#version 450

layout(location=0) in vec2 v_tex_coords;
layout(location=1) in vec2 v_texture_position;
layout(location=2) in vec2 v_texture_size;
layout(location=0) out vec4 f_color;

layout(set = 0, binding = 0) uniform texture2D t_diffuse;
layout(set = 0, binding = 1) uniform sampler s_diffuse;


void main() {

    ivec2 size = textureSize(sampler2D(t_diffuse, s_diffuse), 0);


    vec2 coord = (v_tex_coords * v_texture_size + v_texture_position) / size;


    vec4 color = texture(sampler2D(t_diffuse, s_diffuse), coord);
    if(color.w == 0)
        discard;
    f_color = color;
}