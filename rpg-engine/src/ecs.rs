use std::time::Duration;

use bevy_ecs::{
    component::Component,
    prelude::{Entity, World},
    schedule::{Schedule, Stage, StageLabel},
    system::System,
};
use tracing::debug;
use winit_input_helper::WinitInputHelper;

pub struct CameraActor(Entity);

#[derive(Default)]
pub struct ECS {
    world: World,
    schedule: Schedule,
}

impl ECS {
    pub fn add_stage<S: Stage>(&mut self, label: impl StageLabel, stage: S) -> &mut Self {
        self.schedule.add_stage(label, stage);

        self
    }

    pub fn add_system_to_stage<S: System<In = (), Out = ()>>(
        &mut self,
        stage_label: impl StageLabel,
        system: S,
    ) -> &mut Self {
        self.schedule.add_system_to_stage(stage_label, system);

        self
    }

    pub fn insert_resource<T: Component>(&mut self, resource: T) {
        self.world.insert_resource(resource);
    }

    pub fn stage<T: Stage, F: FnOnce(&mut T) -> &mut T>(
        &mut self,
        label: impl StageLabel,
        func: F,
    ) -> &mut Self {
        self.schedule.stage(label, func);
        self
    }

    pub fn run(&mut self, input: WinitInputHelper, d_time: Duration) {
        self.world.insert_resource(input);
        self.world.insert_resource(d_time);

        self.schedule.run(&mut self.world);
    }

    pub fn get_resource<T>(&mut self) -> Option<&T>
    where
        T: Component,
    {
        self.world.get_resource()
    }

    pub fn debug(&self)
    {
        for (label, _stage) in self.schedule.iter_stages()
        {
            debug!(?label);
        }
    }
}
