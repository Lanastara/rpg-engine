use std::{
    collections::HashMap,
    sync::{
        atomic::{AtomicU32, Ordering},
        Arc,
    },
};

use bevy_ecs::prelude::{Res, ResMut};
use image::DynamicImage;
use parking_lot::{MappedRwLockReadGuard, RwLock, RwLockReadGuard};
use tracing::instrument;
use wgpu::Queue;

use crate::{Handle, Rect, graphics::{backend::Graphics, texture::Texture}};

pub mod uniform;

#[derive(Debug, Hash, PartialEq, Eq, Clone, Copy)]
pub struct TextureAtlasHandle(u32);

#[derive(Clone)]
pub struct TextureAtlases {
    next_handle: Arc<AtomicU32>,
    atlases: Arc<RwLock<HashMap<TextureAtlasHandle, Box<dyn TextureAtlas>>>>,
}

impl Default for TextureAtlases {
    fn default() -> Self {
        Self {
            next_handle: Arc::new(AtomicU32::new(0)),
            atlases: Arc::new(RwLock::new(HashMap::new())),
        }
    }
}

impl TextureAtlases {
    pub fn get(
        &self,
        handle: &TextureAtlasHandle,
    ) -> Option<MappedRwLockReadGuard<Box<dyn TextureAtlas>>> {
        let lock = self.atlases.read();

        let assets = RwLockReadGuard::try_map(lock, |assets| assets.get(handle)).ok();

        assets
    }

    fn get_handle(&self) -> TextureAtlasHandle {
        TextureAtlasHandle(self.next_handle.fetch_add(1, Ordering::SeqCst))
    }

    pub fn insert<TAtlas: TextureAtlas>(&self, atlas: TAtlas) -> TextureAtlasHandle {
        let mut lock = self.atlases.write();

        let handle = self.get_handle();

        lock.insert(handle, Box::new(atlas));

        handle
    }

    fn update(&mut self, queue: &wgpu::Queue) {
        let mut lock = self.atlases.write();
        for (_handle, atlas) in lock.iter_mut() {
            atlas.update(queue);
        }
    }
}

pub trait TextureAtlas : Send + Sync + 'static {
    fn get_texture_position(&self, handle: Handle<DynamicImage>) -> Option<Rect<u16>>;
    fn get_loaded_texture(&self, handle: Handle<DynamicImage>) -> Option<Rect<u16>>;
    fn update(&mut self, queue: &Queue) -> bool;
    fn get_texture(&self) -> Handle<Texture>;
}

#[instrument(skip(atlas, graphics))]
pub fn update_texture_atlases(mut atlas: ResMut<TextureAtlases>, graphics: Res<Graphics>) {
    atlas.update(&graphics.queue);
}
