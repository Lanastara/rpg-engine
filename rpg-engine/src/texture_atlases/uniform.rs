use core::panic;
use std::{
    collections::{HashMap, HashSet},
    usize,
};

use image::{DynamicImage, GenericImageView};
use wgpu::{Device, Extent3d, Origin3d, Queue, TextureCopyView};

use crate::{
    assets::{Assets, LoadState},
    graphics::texture::Texture,
    Handle, Rect,
};
use tracing::{error, info, instrument};

use super::TextureAtlas;

pub struct UniformTextureAtlas {
    pending: HashSet<Handle<DynamicImage>>,
    tile_width: u16,
    tile_height: u16,
    assets: Assets,
    positions: HashMap<Handle<DynamicImage>, Rect<u16>>,
    texture: Option<Texture>,
    pub texture_handle: Handle<Texture>,
}

impl UniformTextureAtlas {
    pub fn builder(parameters: (u16, u16)) -> UniformTextureAtlasBuilder {
        UniformTextureAtlasBuilder::new(parameters)
    }

    fn new(
        assets: Assets,
        pending: HashSet<Handle<DynamicImage>>,
        tile_width: u16,
        tile_height: u16,
        device: &Device,
    ) -> Self {
        let num_pixels = pending.len() as f64 * tile_height as f64 * tile_width as f64;
        let tmp_len = num_pixels.sqrt() as u32;
        let px_width = tmp_len.next_power_of_two() as u32;
        let num_tile_width = px_width as u32 / tile_width as u32;
        let num_tile_height = (pending.len() as f32 / num_tile_width as f32).ceil() as u32;
        let px_height = num_tile_height * tile_height as u32;
        let px_height = px_height.next_power_of_two();

        let mut positions = HashMap::new();

        for (index, handle) in pending.iter().enumerate() {
            positions.insert(
                *handle,
                Rect {
                    width: tile_width,
                    height: tile_height,
                    x: (index % num_tile_width as usize) as u16 * tile_width,
                    y: (index / num_tile_width as usize) as u16 * tile_height,
                },
            );
        }

        let texture = Texture::new_buffer(px_width, px_height, Some(""), device);

        let texture_handle = assets.create_asset::<Texture>();

        Self {
            assets,
            tile_width,
            tile_height,
            pending,
            positions,
            texture: Some(texture),
            texture_handle,
        }
    }
}

impl TextureAtlas for UniformTextureAtlas {
    fn get_loaded_texture(&self, handle: Handle<DynamicImage>) -> Option<Rect<u16>> {
        if self.pending.contains(&handle) {
            None
        } else {
            self.get_texture_position(handle)
        }
    }

    fn get_texture_position(&self, handle: Handle<DynamicImage>) -> Option<Rect<u16>> {
        self.positions.get(&handle).cloned()
    }

    fn get_texture(&self) -> Handle<Texture> {
        self.texture_handle
    }

    #[instrument(skip(self, queue))]
    fn update(&mut self, queue: &Queue) -> bool {
        if !self.pending.is_empty() {
            let mut finished = HashSet::new();

            for handle in self.pending.iter() {
                if let Some(lock) = self.assets.get(handle) {
                    let state: &LoadState<_> = &lock;
                    match state {
                        LoadState::Loaded(img) => {
                            if let Some(pos) = self.positions.get(handle) {
                                if let Some(texture) = &self.texture {
                                    match img.as_rgba8() {
                                        Some(rgba) => {
                                            let dimensions = img.dimensions();

                                            assert_eq!(dimensions.0, self.tile_width as u32);
                                            assert_eq!(dimensions.1, self.tile_height as u32);

                                            queue.write_texture(
                                                TextureCopyView {
                                                    texture: &texture.texture,
                                                    mip_level: 0,
                                                    origin: Origin3d {
                                                        x: pos.x as u32,
                                                        y: pos.y as u32,
                                                        z: 0,
                                                    },
                                                },
                                                rgba,
                                                wgpu::TextureDataLayout {
                                                    offset: 0,
                                                    bytes_per_row: 4 * dimensions.0,
                                                    rows_per_image: dimensions.1,
                                                },
                                                Extent3d {
                                                    width: dimensions.0,
                                                    height: dimensions.1,
                                                    depth: 1,
                                                },
                                            );
                                            finished.insert(*handle);
                                        }
                                        None => {
                                            error!("invalid pixel format");
                                        }
                                    };

                                    //let rgba = .unwrap();
                                } else {
                                    panic!()
                                }
                            }
                        }
                        _ => {}
                    }
                }
            }

            self.pending = self.pending.difference(&finished).cloned().collect();

            if self.pending.is_empty() {
                let asset_ref = self.assets.get_mut(&self.texture_handle);

                if let Some(mut asset_ref) = asset_ref {
                    let texture = self.texture.take();

                    if let Some(texture) = texture {
                        info!("TextureAtlas Loaded");
                        self.texture = None;
                        *asset_ref = LoadState::Loaded(texture);
                        return true;
                    }
                }
            }
        }
        false
    }
}

pub struct UniformTextureAtlasBuilder {
    entries: HashSet<Handle<DynamicImage>>,
    tile_width: u16,
    tile_height: u16,
}

impl UniformTextureAtlasBuilder {
    fn new((tile_width, tile_height): (u16, u16)) -> Self {
        Self {
            tile_width,
            tile_height,
            entries: HashSet::new(),
        }
    }

    pub fn add_texture(&mut self, handle: Handle<DynamicImage>) -> &mut Self {
        self.entries.insert(handle);

        self
    }

    pub fn build(self, assets: Assets, device: &Device) -> UniformTextureAtlas {
        UniformTextureAtlas::new(
            assets,
            self.entries,
            self.tile_height,
            self.tile_width,
            device,
        )
    }
}
