use std::marker::PhantomData;

pub use bevy_ecs;
pub use cgmath;
pub use image;

use bevy_ecs::{component::Component, schedule::Stage, system::IntoSystem};
use bevy_ecs::{
    prelude::System,
    schedule::{RunOnce, Schedule, StageLabel, SystemStage},
};
use ecs::ECS;
use texture_atlases::TextureAtlases;
use winit::{
    event_loop::{ControlFlow, EventLoop},
    window::{Window, WindowBuilder},
};

pub use winit::event::VirtualKeyCode;
pub use winit_input_helper::WinitInputHelper;
pub mod animations;
pub mod assets;
pub mod ecs;
pub mod graphics;
pub mod texture_atlases;

type HandleId = u32;

pub struct Handle<TAsset>(HandleId, PhantomData<TAsset>);

impl<TAsset> Default for Handle<TAsset> {
    fn default() -> Self {
        Self(0, PhantomData::default())
    }
}

#[derive(Debug, Clone)]
pub struct Rect<T> {
    pub x: T,
    pub y: T,
    pub width: T,
    pub height: T,
}
#[derive(Debug)]
pub struct WindowSize {
    pub width: u32,
    pub height: u32,
}

#[derive(Debug, Hash, PartialEq, Eq, Clone, StageLabel)]
pub enum MainStage {
    Startup,
    Script,
    Input,
    Prepare,
    Update,
    Render,
    Cleanup,
}

#[derive(Debug, Hash, PartialEq, Eq, Clone, StageLabel)]
pub enum StartupStage {
    PreStartup,
    Startup,
    PostStartup,
}

#[derive(Debug, Hash, PartialEq, Eq, Clone, StageLabel)]
pub enum RenderStage {
    PreRender,
    Render,
    PostRender,
}

#[derive(Debug, Hash, PartialEq, Eq, Clone, StageLabel)]
pub enum UpdateStage {
    PreUpdate,
    Update,
    PostUpdate,
}

pub struct App {
    input: WinitInputHelper,
    event_loop: EventLoop<()>,
    window: Window,
    ecs: ecs::ECS,
}

impl App {
    pub fn builder() -> AppBuilder {
        AppBuilder::new()
    }

    pub fn run(self) {
        let mut last_frame = std::time::Instant::now();

        let Self {
            mut input,
            mut ecs,
            event_loop,
            window,
        } = self;

        ecs.insert_resource(window);

        event_loop.run(move |event, _, control_flow| {
            if input.update(&event) {
                if let Some(size) = input.window_resized() {
                    ecs.insert_resource(WindowSize {
                        width: size.width,
                        height: size.height,
                    });
                }
                if let Some(_factor) = input.scale_factor_changed() {
                    // Probaly do something
                }

                let d_time = std::time::Instant::now() - last_frame;
                last_frame = std::time::Instant::now();

                ecs.run(input.clone(), d_time);
            }

            if input.quit() {
                *control_flow = ControlFlow::Exit;
            }
        });
    }
}

pub struct AppBuilder {
    input: WinitInputHelper,
    event_loop: EventLoop<()>,
    window: Window,
    ecs: ECS,
}

impl AppBuilder {
    fn new() -> Self {
        let input = WinitInputHelper::new();

        let event_loop = EventLoop::new();
        let window = WindowBuilder::new().build(&event_loop).unwrap();

        Self {
            ecs: ECS::default(),
            input,
            event_loop,
            window,
        }
    }

    pub fn build(self) -> App {
        self.ecs.debug();

        App {
            input: self.input,
            event_loop: self.event_loop,
            window: self.window,
            ecs: self.ecs,
        }
    }

    pub async fn add_default_systems(mut self) -> Self {
        self.ecs
            .add_stage(
                MainStage::Startup,
                Schedule::default()
                    .with_run_criteria(RunOnce::default())
                    .with_stage(StartupStage::PreStartup, SystemStage::parallel())
                    .with_stage(StartupStage::Startup, SystemStage::parallel())
                    .with_stage(StartupStage::PostStartup, SystemStage::parallel()),
            )
            .add_stage(MainStage::Script, SystemStage::parallel())
            .add_stage(MainStage::Input, SystemStage::parallel())
            .add_stage(MainStage::Prepare, SystemStage::parallel())
            .add_stage(
                MainStage::Update,
                Schedule::default()
                    .with_stage(UpdateStage::PreUpdate, SystemStage::parallel())
                    .with_stage(UpdateStage::Update, SystemStage::parallel())
                    .with_stage(UpdateStage::PostUpdate, SystemStage::parallel()),
            )
            .stage(MainStage::Update, |schedule: &mut Schedule| {
                schedule.add_system_to_stage(UpdateStage::PreUpdate, animations::animation.system())
            })
            .add_stage(
                MainStage::Render,
                Schedule::default()
                    .with_stage(RenderStage::PreRender, SystemStage::parallel())
                    .with_stage(RenderStage::Render, SystemStage::parallel())
                    .with_stage(RenderStage::PostRender, SystemStage::parallel()),
            )
            .add_stage(MainStage::Cleanup, SystemStage::parallel())
            .stage(MainStage::Render, |schedule: &mut Schedule| {
                schedule
                    .add_system_to_stage(
                        RenderStage::PreRender,
                        texture_atlases::update_texture_atlases.system(),
                    )
                    .add_system_to_stage(
                        RenderStage::PreRender,
                        graphics::resolve_texture_atlas.system(),
                    )
                    .add_system_to_stage(
                        RenderStage::PreRender,
                        graphics::backend::update_size.system(),
                    )
                    .add_system_to_stage(
                        RenderStage::Render,
                        graphics::backend::render.system(),
                    )
            });

        self.ecs.insert_resource(WindowSize {
            width: self.window.inner_size().width,
            height: self.window.inner_size().height,
        });

        let assets = assets::Assets::default();

        self.ecs.insert_resource(assets.clone());

        let server = assets::AssetServer::builder().register_loader(assets::ImageLoader);

        self.ecs.insert_resource(server.build(assets));

        self.ecs.insert_resource(
            graphics::backend::Graphics::new(&self.window)
                .await
                .unwrap(),
        );

        self.ecs.insert_resource(TextureAtlases::default());

        self
    }

    pub fn add_system_to_stage<S: System<In = (), Out = ()>>(
        mut self,
        stage_label: impl StageLabel,
        system: S,
    ) -> Self {
        self.ecs.add_system_to_stage(stage_label, system);

        self
    }

    pub fn insert_resource<T: Component>(mut self, resource: T) -> Self {
        self.ecs.insert_resource(resource);

        self
    }

    pub fn stage<T: Stage, F: FnOnce(&mut T) -> &mut T>(
        mut self,
        label: impl StageLabel,
        func: F,
    ) -> Self {
        self.ecs.stage(label, func);
        self
    }
}
