use std::{
    collections::HashMap,
    hash::Hash,
    marker::PhantomData,
    sync::{
        atomic::{AtomicU32, Ordering},
        Arc,
    },
};

use crate::Handle;
use anymap::{any::Any, Map};
use bevy_tasks::TaskPoolBuilder;
use crossbeam_channel::{Receiver, Sender};
use image::DynamicImage;
use parking_lot::{
    MappedRwLockReadGuard, MappedRwLockWriteGuard, RwLock, RwLockReadGuard, RwLockWriteGuard,
};
use tracing::{error, info, instrument, Instrument};

impl Asset for DynamicImage {
    type Loader = ImageLoader;
}

pub struct ImageLoader;

impl AssetLoader for ImageLoader {
    fn load(&self, path: &str) -> Option<DynamicImage> {
        image::open(path).ok()
    }

    type Output = DynamicImage;
}

impl<TAsset> Handle<TAsset> {
    pub fn none() -> Self {
        Self(0, PhantomData::default())
    }
}

impl<T> std::fmt::Debug for Handle<T> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}

impl<T> PartialEq for Handle<T> {
    fn eq(&self, other: &Self) -> bool {
        self.0.eq(&other.0)
    }
}

impl<T> Clone for Handle<T> {
    fn clone(&self) -> Self {
        Self(self.0.clone(), PhantomData::default())
    }
}

impl<T> Copy for Handle<T> {}

impl<T> Eq for Handle<T> {}

impl<T> Hash for Handle<T> {
    fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
        self.0.hash(state)
    }
}

pub trait BaseAssetLoader: Any + Send + Sync {}

pub trait AssetLoader: BaseAssetLoader {
    type Output: Asset;

    fn load(&self, path: &str) -> Option<Self::Output>;
}

impl<T> BaseAssetLoader for T where T: AssetLoader {}

pub trait Asset: Any + Send + Sync {
    type Loader: AssetLoader<Output = Self>;
}

#[derive(Debug, Clone)]
pub struct Assets {
    next_handle: Arc<AtomicU32>,
    assets: Arc<RwLock<Map<dyn Any + Send + Sync>>>,
}

impl Default for Assets {
    fn default() -> Self {
        Self {
            next_handle: Arc::new(AtomicU32::new(0)),
            assets: Arc::new(RwLock::new(Map::new())),
        }
    }
}

impl Assets {
    pub fn get<THandle: Send + Sync + 'static>(
        &self,
        handle: &Handle<THandle>,
    ) -> Option<MappedRwLockReadGuard<LoadState<THandle>>> {
        let lock = self.assets.read();

        let assets = RwLockReadGuard::try_map(lock, |assets| {
            let assets = assets.get::<HashMap<Handle<THandle>, LoadState<THandle>>>();
            if let Some(assets) = assets {
                assets.get(handle)
            } else {
                None
            }
        })
        .ok();

        assets
    }

    pub fn get_mut<THandle: Send + Sync + 'static>(
        &mut self,
        handle: &Handle<THandle>,
    ) -> Option<MappedRwLockWriteGuard<LoadState<THandle>>> {
        let lock = self.assets.write();

        let assets = RwLockWriteGuard::try_map(lock, |assets| {
            let assets = assets.get_mut::<HashMap<Handle<THandle>, LoadState<THandle>>>();
            if let Some(assets) = assets {
                assets.get_mut(handle)
            } else {
                None
            }
        })
        .ok();

        assets
    }

    fn get_handle<T>(&self) -> Handle<T> {
        Handle(
            self.next_handle.fetch_add(1, Ordering::SeqCst),
            PhantomData::default(),
        )
    }

    pub fn create_asset<THandle: Send + Sync + 'static>(&self) -> Handle<THandle> {
        let mut lock = self.assets.write();

        let handle = self.get_handle();

        if let Some(assets) = lock.get_mut::<HashMap<Handle<THandle>, LoadState<THandle>>>() {
            assets.insert(handle, LoadState::Loading);
        } else {
            let mut assets: HashMap<Handle<THandle>, LoadState<THandle>> = HashMap::new();
            assets.insert(handle, LoadState::Loading);
            lock.insert(assets);
        }

        handle
    }
}

#[derive(Debug, Clone)]
pub enum LoadState<T> {
    Loading,
    Error,
    Loaded(T),
}

pub struct AssetServerBuilder {
    loaders: Map<dyn Any + Send + Sync + 'static>,
}

impl AssetServerBuilder {
    pub fn register_loader<T: BaseAssetLoader + 'static>(mut self, loader: T) -> Self {
        self.loaders.insert(loader);

        self
    }

    pub fn build(self, assets: Assets) -> AssetServer {
        let (sender, receiver) = crossbeam_channel::unbounded();

        AssetServer {
            assets,
            threadpool: TaskPoolBuilder::new().build(),
            asset_loaders: Arc::new(self.loaders),
            sender,
            receiver,
        }
    }
}

pub enum AssetEvent {
    StartLoading,
    Finished,
}

pub struct AssetServer {
    assets: Assets,
    threadpool: bevy_tasks::TaskPool,
    asset_loaders: Arc<Map<dyn Any + Send + Sync>>,
    sender: Sender<AssetEvent>,
    receiver: Receiver<AssetEvent>,
}

impl AssetServer {
    pub fn builder() -> AssetServerBuilder {
        AssetServerBuilder {
            loaders: Map::new(),
        }
    }

    #[instrument(skip(self))]
    pub fn load<T: Asset>(&mut self, path: &str) -> Handle<T> {
        let mut assets = self.assets.clone();
        let loaders = self.asset_loaders.clone();
        let path = path.to_string();

        let handle = assets.create_asset::<T>();

        let sender = self.sender.clone();

        self.threadpool
            .spawn(
                async move {
                    sender.send(AssetEvent::StartLoading).unwrap();

                    if let Some(loader) = loaders.get::<T::Loader>() {
                        let asset = loader.load(&path);

                        if let Some(asset) = asset {
                            let asset_ref = assets.get_mut(&handle);

                            if let Some(mut asset_ref) = asset_ref {
                                *asset_ref = LoadState::Loaded(asset);
                                info!("Asset Loaded");
                            }
                        } else {
                            error!(?path, "Error while loading Asset");
                        }
                    }

                    sender.send(AssetEvent::Finished).unwrap();
                }
                .instrument(tracing::info_span!("load_asset future")),
            )
            .detach();

        handle
    }

    pub fn try_recv(&mut self) -> Option<AssetEvent> {
        self.receiver.try_recv().ok()
    }
}
